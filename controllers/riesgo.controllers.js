var fs = require('fs');
const Riesgo = require("../models/riesgo.model.js");


/*************************************************************************/
/******************************* F A S T *********************************/
/*************************************************************************/

// Crear un Historial_ticket
exports.getAlumnosRiesgoFast = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  // console.log(req.body.ciclo)
  Riesgo.getAlumnosRiesgoFast(req.body.ciclo, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}

// Crear un Historial_ticket
exports.getAlumnosRiesgoPagoFast = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  // console.log(req.body.ciclo)
  Riesgo.getAlumnosRiesgoPagoFast(req.body.iderp, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}

// Crear un Historial_ticket
exports.getAlumnosRiesgoInbi = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  // console.log(req.body.ciclo)
  Riesgo.getAlumnosRiesgoInbi(req.body.ciclo, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}

exports.getCiclosFast = (req,res) => {
    Riesgo.getCiclosFast((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};


// GRUPOS POR TEACHER
exports.getGruposTeacherFast = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  Riesgo.getGruposTeacherFast(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}

// EJERCICIOS FALTANTES
exports.getRecursoFaltaFast = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  Riesgo.getRecursoFaltaFast(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}

/*************************************************************************/
/******************************** I N B I ********************************/
/*************************************************************************/

// Crear un Historial_ticket
exports.getAlumnosRiesgoFastCalif = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  // console.log(req.body.ciclo)
  Riesgo.getAlumnosRiesgoFastCalif(req.body.ciclo, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};


exports.getAlumnosRiesgoInbiCalif = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  // console.log(req.body.ciclo)
  Riesgo.getAlumnosRiesgoInbiCalif(req.body.ciclo, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

exports.getCiclosInbi = (req,res) => {
    Riesgo.getCiclosInbi((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};

// GRUPOS POR TEACHER
exports.getGruposTeacherInbi = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  Riesgo.getGruposTeacherInbi(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}



exports.getRecursoFaltaInbi = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  Riesgo.getRecursoFaltaInbi(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
}
