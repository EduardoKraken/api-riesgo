const { result } = require("lodash");
//modificar aca
const sqlFast = require("../db.js");
const sqlInbi = require("../db2.js");
//const constructor
const Archivos = function(archivos) {};

/////imagenes de cada grupo////////////
// fast
Archivos.getArchivosGrupo = result => {
    sqlFast.query(`SELECT * FROM imagenes_grupos WHERE deleted = 0 and aprobada=0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("archivos: ", res);
        result(null, res);

    });
};


Archivos.updateArchivoGrupo = (id, c, result) => {
    sqlFast.query(` UPDATE imagenes_grupos SET usuario_reviso_erp=?, aprobada=?, fecha_actualizo=?,deleted=? WHERE idimagenes_grupo=?`, [c.usuario_reviso_erp, c.aprobada, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("imagen actualizada: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}


//// I N B I 
/////imagenes de cada grupo////////////

Archivos.getArchivosGrupoInbi = result => {
    sqlInbi.query(`SELECT * FROM imagenes_grupos WHERE deleted = 0 and aprobada=0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("archivos: ", res);
        result(null, res);

    });
};


Archivos.updateArchivoGrupoInbi = (id, c, result) => {
    sqlInbi.query(` UPDATE imagenes_grupos SET usuario_reviso_erp=?, aprobada=?, fecha_actualizo=?,deleted=? WHERE idimagenes_grupo=?`, [c.usuario_reviso_erp, c.aprobada, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("imagen actualizada: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}


module.exports = Archivos;