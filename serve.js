// IMPORTAR DEPENDENCIAS
const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');
const fileUpload = require('express-fileupload');

// IMPORTAR EXPRESS
const app = express();

//Para servidor con ssl
var http = require('http');
const https = require('https');
const fs = require('fs');

// Rutas estaticas
app.use('/fotos', express.static('../fotos'));
app.use('/pdfs', express.static('../pdf'));

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos

// ----IMPORTAR RUTAS---------------------------------------->
// var r_users          = require('./routes/users.routes');
require('./routes/areas.routes')(app);
require('./routes/historial_ticket.routes')(app);
require('./routes/usuarios.routes')(app);
require('./routes/asistencias.routes')(app);
require('./routes/ticket.routes')(app);
require('./routes/documentos.routes')(app);
require('./routes/riesgo.routes')(app);
require("./routes/kpi/reportes.routes")(app);

//imagenes grupos
require('./routes/lms/archivos.routes')(app);
// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3005, () => {
    console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
    console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
    console.log("|**************************************************************| ");
    console.log("|************ Servidor Corriendo en el Puerto 3005 ************| ");
});