const fast = require("./db.js");
const inbi = require("./db2.js");
const erp = require("./db3.js");


// constructor
const Asistencias = function(clientes) {
  this.idweb     = clientes.idweb;
  this.nomcli    = clientes.nomcli;
  this.calle     = clientes.calle;
};


Asistencias.getAsistencias = (result)=>{
	fast.query(`SELECT ag.*, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              WHERE valor_asistencia = 2 AND g.id_ciclo = 17;`,(err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};

Asistencias.getHistorial = (result)=>{
  erp.query(`SELECT * FROM historial_asistencia;`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};

Asistencias.getAsistenciasPlantel = (plantel,ciclo,result)=>{
  fast.query(`SELECT ag.*,da.telefono, da.celular,ag.fecha_creacion,ag.estatus_llamada, 1 AS unidad_negocio, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              LEFT JOIN datos_alumno da ON da.id_usuario = u.id
              WHERE valor_asistencia = 2 AND ag.estatus_llamada IN(0,1,4) AND g.id_plantel = ? AND g.id_ciclo = ?`,[plantel,ciclo],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};

Asistencias.getAsistenciasFast = (result)=>{
  fast.query(`SELECT ag.*,da.telefono, da.celular,ag.fecha_creacion,ag.estatus_llamada, 1 AS unidad_negocio, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              LEFT JOIN datos_alumno da ON da.id_usuario = u.id
              WHERE valor_asistencia = 2 AND g.id_ciclo = (SELECT id FROM ciclos WHERE CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin AND estatus = 1 AND deleted = 0 LIMIT 1);`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};


Asistencias.getAsistenciasInbi = (result)=>{
  fast.query(`SELECT ag.*,da.telefono, da.celular,ag.fecha_creacion,ag.estatus_llamada, 1 AS unidad_negocio, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              LEFT JOIN datos_alumno da ON da.id_usuario = u.id
              WHERE valor_asistencia = 2 AND g.id_ciclo = (SELECT id FROM ciclos WHERE CURRENT_DATE BETWEEN fecha_inicio AND fecha_fin AND estatus = 1 AND deleted = 0 LIMIT 1);`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};


Asistencias.getAsistenciasPlantelINBI = (plantel,ciclo,result)=>{
  inbi.query(`SELECT ag.*,da.telefono, da.celular,ag.fecha_creacion,ag.estatus_llamada, 0 AS unidad_negocio, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ",IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              LEFT JOIN datos_alumno da ON da.id_usuario = u.id
              WHERE valor_asistencia = 2 AND ag.estatus_llamada IN(0,1,4) AND g.id_plantel = ? AND g.id_ciclo = ?;`,[plantel,ciclo],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};


Asistencias.putAsistencia = (id, sub, result) => {
  fast.query(` UPDATE asistencia_grupo SET estatus_llamada = ? WHERE id = ?`,
   [sub.estatus,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated sub: ", { id: id, ...sub });
      result(null, { id: id, ...sub });
    }
  );
};

Asistencias.putAsistenciaINBI = (id, sub, result) => {
  inbi.query(` UPDATE asistencia_grupo SET estatus_llamada = ? WHERE id = ?`,
   [sub.estatus,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated sub: ", { id: id, ...sub });
      result(null, { id: id, ...sub });
    }
  );
};

Asistencias.addHistorial = (c, result) => {
  erp.query(`INSERT INTO historial_asistencia(id_alumno, id_usuario, nota,unidad_negocio, id_falta,intento_llamada,fecha_asistencia,tipo_contacto)VALUES(?,?,?,?,?,?,?,?)`,
    [c.id_alumno,c.id_usuario,c.nota,c.unidad_negocio,c.id_falta,c.intento_llamada,c.fecha_asistencia,c.tipo_contacto],(err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Area: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};


Asistencias.getHistorialEscuela = (result)=>{
  erp.query(`SELECT 
              (SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 1 AND intento_llamada = 1) AS intento_fast,
              (SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 1 AND intento_llamada = 0) AS contacto_fast,
              (SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 0 AND intento_llamada = 1) AS intento_inbi,
              (SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 0 AND intento_llamada = 0) AS contacto_inbi
            FROM historial_asistencia LIMIT 1;`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res[0]);
  })
};

Asistencias.getHistorialSucursal = (result)=>{
  erp.query(`SELECT h.id_usuario, u.id_plantel,  p.plantel, h.unidad_negocio,
(SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 1 AND intento_llamada = 1 AND id_usuario = h.id_usuario) AS intento_fast,
(SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 1 AND intento_llamada = 0 AND id_usuario = h.id_usuario) AS contacto_fast,
(SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 0 AND intento_llamada = 1 AND id_usuario = h.id_usuario) AS intento_inbi,
(SELECT count(*) FROM historial_asistencia WHERE unidad_negocio = 0 AND intento_llamada = 0 AND id_usuario = h.id_usuario) AS contacto_inbi
FROM historial_asistencia h
LEFT JOIN usuarios u ON u.id_usuario = h.id_usuario
LEFT JOIN planteles p ON p.id_plantel = u.id_plantel
GROUP BY h.id_usuario;`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};


Asistencias.getAllAsistencias = (result)=>{
  fast.query(`SELECT CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo, u.id, g.id_unidad_negocio FROM usuarios u
              LEFT JOIN grupo_alumnos ga ON u.id = ga.id_alumno
              LEFT JOIN grupos g ON g.id = ga.id_grupo
              WHERE id_tipo_usuario = 1;`,(err,dataFast)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    inbi.query(`SELECT CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo, u.id, g.id_unidad_negocio FROM usuarios u
              LEFT JOIN grupo_alumnos ga ON u.id = ga.id_alumno
              LEFT JOIN grupos g ON g.id = ga.id_grupo
              WHERE id_tipo_usuario = 1;`,(err,dataInbi)=>{
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }      
      result(null,dataFast);
    })
  })
};

/*****************************************************************************************/
Asistencias.getAsistenciasFastEstatus = (result)=>{
  fast.query(`SELECT ag.*,da.telefono, da.celular,ag.fecha_creacion,ag.estatus_llamada, 1 AS unidad_negocio, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              LEFT JOIN datos_alumno da ON da.id_usuario = u.id
              WHERE ag.estatus_llamada = 3;`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};


Asistencias.getAsistenciasInbiEstatus = (result)=>{
  inbi.query(`SELECT ag.*,da.telefono, da.celular,ag.fecha_creacion,ag.estatus_llamada, 1 AS unidad_negocio, CONCAT(u.nombre," ",IFNULL(u.apellido_paterno,"")," ", IFNULL(u.apellido_materno,"")) AS nombre, g.nombre AS grupo FROM asistencia_grupo ag 
              LEFT JOIN usuarios u ON u.id = ag.id_usuario
              LEFT JOIN grupos g ON g.id = ag.id_grupo
              LEFT JOIN datos_alumno da ON da.id_usuario = u.id
              WHERE ag.estatus_llamada = 3;`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};

Asistencias.getAsistenciaAlumno = (data,result)=>{
  erp.query(`SELECT * FROM historial_asistencia WHERE id_alumno = ? AND id_falta = ?;`,[data.id_alumno, data.id_falta],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
};


Asistencias.getTeachers = (idciclo, result)=>{
  fast.query(`SELECT g.id, g.nombre,
    (SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher FROM grupo_teachers gt
    LEFT JOIN usuarios u ON  u.id = gt.id_teacher WHERE gt.id_grupo = g.id) AS teacher1,
    (SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher FROM grupo_teachers gt
    LEFT JOIN usuarios u ON  u.id = gt.id_teacher_2 WHERE gt.id_grupo = g.id) AS teacher2
    FROM grupos g WHERE g.id_ciclo = ?;`,[idciclo],(error, res)=>{
    if (error) {
      console.log("error: ", error);
      result(null, error);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
}

Asistencias.getTeachersInbi = (idciclo, result)=>{
  inbi.query(`SELECT g.id, g.nombre,
    (SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher FROM grupo_teachers gt
    LEFT JOIN usuarios u ON  u.id = gt.id_teacher WHERE gt.id_grupo = g.id) AS teacher1,
    (SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher FROM grupo_teachers gt
    LEFT JOIN usuarios u ON  u.id = gt.id_teacher_2 WHERE gt.id_grupo = g.id) AS teacher2
    FROM grupos g WHERE g.id_ciclo = ?;`,[idciclo],(error, res)=>{
    if (error) {
      console.log("error: ", error);
      result(null, error);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
}

module.exports = Asistencias;