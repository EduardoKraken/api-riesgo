const Archivos = require("../../models/lms/archivos.model.js");

//imagenes por grupo FAST


exports.getArchivosGrupo = (req, res) => {
    Archivos.getArchivosGrupo((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los archivos"
            });
        else res.send(data);
    });
};

exports.updateArchivoGrupo = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Archivos.updateArchivoGrupo(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el archivo con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el archivo con el id : " + req.params.id });
            }
        } else {
            res.status(200).send(data)
        }
    })
}

//imagenes por grupo FAST

exports.getArchivosGrupoInbi = (req, res) => {
    Archivos.getArchivosGrupoInbi((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los archivos"
            });
        else res.send(data);
    });
};

exports.updateArchivoGrupoInbi = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Archivos.updateArchivoGrupoInbi(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el archivo con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el archivo con el id : " + req.params.id });
            }
        } else {
            res.status(200).send(data)
        }
    })
}