module.exports = app => {
  const asistencias = require('../controllers/asistencias.controllers') // --> ADDED THIS
  app.get("/asistencias.all",     					               asistencias.getAsistencias);
  app.get("/asistencias.plantel/:idplantel/:ciclo", 		   asistencias.getAsistenciasPlantel);
  app.get("/asistencias.plantel.inbi/:idplantel/:ciclo", 	 asistencias.getAsistenciasPlantelINBI);
  app.put("/asistencias.update/:id",         				       asistencias.putAsistencia);
  app.put("/asistencias.update.inbi/:id", 			           asistencias.putAsistenciaINBI);
  app.post("/asistencias.historial",	 			               asistencias.addHistorial);

  // Reportes
  app.get("/asistencias.historial.escuela",          asistencias.getHistorialEscuela);
  app.get("/asistencias.historial.sucursal",         asistencias.getHistorialSucursal);

  app.get("/all.asistencias",                        asistencias.getAllAsistencias);

  app.get("/asistencias.fast",                       asistencias.getAsistenciasFast);
  app.get("/asistencias.inbi",                       asistencias.getAsistenciasInbi);
  app.get("/historial",                              asistencias.getHistorial);

  //¨Para la directora 
  app.get("/asistencias.fast.estatus",               asistencias.getAsistenciasFastEstatus);
  app.get("/asistencias.inbi.estatus",               asistencias.getAsistenciasInbiEstatus);
  app.post("/asistencias.alumno",                    asistencias.getAsistenciaAlumno);

  app.get("/get.teachers/:idciclo",                  asistencias.getTeachers);
  app.get("/get.teachers.inbi/:idciclo",             asistencias.getTeachersInbi);
};