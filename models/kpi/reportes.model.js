const { result } = require("lodash");
const sqlFAST  = require("../db.js");
const sqlINBI = require("../db2.js");

//const constructor
const Reportes = function(depas) {};

Reportes.habilitarGroup = () => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Reportes.reporteTeacherAsistenciaFast1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia < CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteTeacherAsistenciaFast2 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher_2
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia < CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Jueves','Viernes')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

/****************************************************************************************************/

Reportes.habilitarGroupInbi = () => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Reportes.reporteTeacherAsistenciaInbi1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia < CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteTeacherAsistenciaInbi2 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher_2
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia < CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Jueves','Viernes')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

/****************************************************************************************************/

Reportes.alumosCicloSiguiente = (inbi,fast,sigInbi,sigFast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      AND a.id_alumno IN (SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY g.id_grupo)
      GROUP BY g.id_grupo;`,[inbi,fast,sigInbi,sigFast], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      console.log(res.length)
      resolve(res)
    });
  });
};

/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

/*FAST */
Reportes.getNumberDia = () => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT id,(ELT(WEEKDAY(CURDATE()) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo')) AS dia FROM ciclos WHERE NOW() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

Reportes.getGruposBloqueA = (ciclo) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre, h.hora_inicio, TIME(NOW()) AS hora, TIMEDIFF(h.hora_inicio,TIME(NOW())) AS diferencia, u.id,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher,
      IFNULL((SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS nombre FROM asistencia_teacher ate 
      LEFT JOIN usuarios u ON u.id = ate.id_usuario WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND  CONCAT(CURDATE(), " 23:25:59") AND ate.id_usuario = gt.id_teacher
      LIMIT 1),"") AS asistencia
      FROM grupos g 
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      WHERE g.id_ciclo = ?  AND TIMEDIFF(h.hora_inicio,TIME(NOW())) < "00:30" AND TIMEDIFF(h.hora_inicio,TIME(NOW())) > "00:00" AND g.optimizado = 0
      AND g.id IN (
      SELECT gg.id FROM grupo_teachers gtt 
      LEFT JOIN grupos gg ON gg.id = gtt.id_grupo 
      LEFT JOIN horarios hh ON hh.id = gg.id_horario 
      WHERE gtt.id_teacher = gt.id_teacher AND gg.id_ciclo = ? AND gg.optimizado = 0
      ORDER BY hh.hora_inicio );`,[ciclo,ciclo],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Reportes.getGruposBloqueB = (ciclo) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre, h.hora_inicio, TIME(NOW()) AS hora, TIMEDIFF(h.hora_inicio,TIME(NOW())) AS diferencia, u.id,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher,
      IFNULL((SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS nombre FROM asistencia_teacher ate 
      LEFT JOIN usuarios u ON u.id = ate.id_usuario WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND  CONCAT(CURDATE(), " 23:25:59") AND ate.id_usuario = gt.id_teacher_2
      LIMIT 1),"") AS asistencia
      FROM grupos g 
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher_2
      WHERE g.id_ciclo = ?  AND TIMEDIFF(h.hora_inicio,TIME(NOW())) < "00:30" AND TIMEDIFF(h.hora_inicio,TIME(NOW())) > "00:00" AND g.optimizado = 0
      AND g.id g.id IN (
      SELECT gg.id FROM grupo_teachers gtt 
      LEFT JOIN grupos gg ON gg.id = gtt.id_grupo 
      LEFT JOIN horarios hh ON hh.id = gg.id_horario 
      WHERE gtt.id_teacher_2 = gt.id_teacher_2 AND gg.id_ciclo = ? AND gg.optimizado = 0
      ORDER BY hh.hora_inicio );`,[ciclo,ciclo],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};


/* INBI */
Reportes.getNumberDiaInbi = () => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT id,(ELT(WEEKDAY(CURDATE()) + 1, '1', '2', '3', '4', '5', '6', '7')) AS dia FROM ciclos WHERE NOW() BETWEEN fecha_inicio AND fecha_fin AND deleted = 0;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      console.log(res.length)
      resolve(res[0])
    });
  });
};


Reportes.getGruposBloqueAInbi = (ciclo, dia) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre, h.hora_inicio, TIME(NOW()) AS hora, TIMEDIFF(h.hora_inicio,TIME(NOW())) AS diferencia, u.id,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher,
      IFNULL((SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS nombre FROM asistencia_teacher ate 
      LEFT JOIN usuarios u ON u.id = ate.id_usuario WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND  CONCAT(CURDATE(), " 23:25:59") AND ate.id_usuario = gt.id_teacher
      ),"") AS asistencia
      FROM grupos g 
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher
      WHERE g.id_ciclo = ?  AND TIMEDIFF(h.hora_inicio,TIME(NOW())) < "00:30" AND TIMEDIFF(h.hora_inicio,TIME(NOW())) > "00:00" AND g.optimizado = 0
      AND g.id IN (
      SELECT gg.id FROM grupo_teachers gtt 
      LEFT JOIN grupos gg ON gg.id = gtt.id_grupo 
      LEFT JOIN horarios hh ON hh.id = gg.id_horario 
      WHERE gtt.id_teacher = gt.id_teacher AND gg.id_ciclo = ? AND gg.optimizado = 0
      ORDER BY hh.hora_inicio );`,[ciclo,ciclo],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Reportes.getGruposBloqueBInbi = (ciclo) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre, h.hora_inicio, TIME(NOW()) AS hora, TIMEDIFF(h.hora_inicio,TIME(NOW())) AS diferencia, u.id,
      CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher,
      IFNULL((SELECT CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS nombre FROM asistencia_teacher ate 
      LEFT JOIN usuarios u ON u.id = ate.id_usuario WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND  CONCAT(CURDATE(), " 23:25:59") AND ate.id_usuario = gt.id_teacher_2
      ),"") AS asistencia
      FROM grupos g 
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN grupo_teachers gt ON gt.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt.id_teacher_2
      WHERE g.id_ciclo = ?  AND TIMEDIFF(h.hora_inicio,TIME(NOW())) < "00:30" AND TIMEDIFF(h.hora_inicio,TIME(NOW())) > "00:00" AND g.optimizado = 0
      AND g.id IN (
      SELECT gg.id FROM grupo_teachers gtt 
      LEFT JOIN grupos gg ON gg.id = gtt.id_grupo 
      LEFT JOIN horarios hh ON hh.id = gg.id_horario 
      WHERE gtt.id_teacher_2 = gt.id_teacher_2 AND gg.id_ciclo = ? AND gg.optimizado = 0
      ORDER BY hh.hora_inicio );`,[ciclo,ciclo],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

/*****************************************************************************************************/
/*****************************************************************************************************/

Reportes.reporteAsistenciaTeacherFast1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteAsistenciaTeacherFast2 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Jueves','Viernes')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteAsistenciaTeacherInbi1 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Lunes','Martes','Miercoles','Sabado', 'Domingo')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

Reportes.reporteAsistenciaTeacherInbi2 = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre,g.id_curso,CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno,"")) AS teacher, a.fecha_asistencia,
      (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS dia, cu.nombre AS curso
      FROM asistencia_grupo a
      LEFT JOIN grupos g ON g.id = a.id_grupo 
      LEFT JOIN grupo_teachers gt1 ON gt1.id_grupo = g.id
      LEFT JOIN usuarios u ON u.id = gt1.id_teacher
      LEFT JOIN ciclos c ON c.id = g.id_ciclo
      LEFT JOIN cursos cu ON cu.id = g.id_curso
      WHERE g.id_ciclo = ? AND g.optimizado = 0 AND g.deleted = 0 AND a.valor_asistencia = 0 AND  a.fecha_asistencia <= CURDATE() 
      AND (ELT(WEEKDAY( a.fecha_asistencia) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) IN ('Jueves','Viernes')
      GROUP BY a.fecha_asistencia, a.id_grupo;`,[actual], (err, cicloActual) => {
      if(err){
        reject(err);
        return;
      }
      resolve(cicloActual)
    });
  });
};

/************************************************************************************************/
/************************************************************************************************/

Reportes.getNumberDiaBD = () => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT (ELT(WEEKDAY(CURDATE()) + 1, 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo')) AS dia;`,(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res[0])
    });
  });
};

Reportes.getAsistenciaDelTeacher = ( actual, dia, hora_inicio ) => {
   return new Promise((resolve,reject)=>{
    sqlFAST.query(`SELECT g.id, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, 
      IF(IFNULL(TIME_FORMAT(TIMEDIFF(TIME(ci.fecha),TIME(h.hora_inicio)),"%H:%i"),"20:00") > "00:10","ALTO","BAJO") AS riesgo,
      IFNULL(CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),"") AS teacher, IFNULL(ci.fecha,"") AS fecha FROM grupos g
      LEFT JOIN cursos c ON c.id = g.id_curso
      LEFT JOIN frecuencia f ON f.id = c.id_frecuencia
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN clase_iniciada ci ON g.id = ci.id_grupo AND ci.fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND  CONCAT(CURDATE(), " 23:25:59")
      LEFT JOIN usuarios u ON u.id = ci.id_teacher
      WHERE g.id_ciclo = ? AND ${ dia } = 1 AND hora_inicio = ?;`,[ actual, hora_inicio ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      console.log(res)
      resolve(res)
    });
  });
};

Reportes.getAsistenciaDelTeacherInbi = ( actual, dia, hora_inicio ) => {
   return new Promise((resolve,reject)=>{
    sqlINBI.query(`SELECT g.id, g.nombre AS grupo, c.nombre AS curso, h.hora_inicio, 
      IF(IFNULL(TIME_FORMAT(TIMEDIFF(TIME(ci.fecha),TIME(h.hora_inicio)),"%H:%i"),"20:00") > "00:10","ALTO","BAJO") AS riesgo,
      IFNULL(CONCAT(u.nombre, " ", u.apellido_paterno, " ", IFNULL(u.apellido_materno, "")),"") AS teacher, IFNULL(ci.fecha,"") AS fecha FROM grupos g
      LEFT JOIN cursos c ON c.id = g.id_curso
      LEFT JOIN frecuencia f ON f.id = c.id_frecuencia
      LEFT JOIN horarios h ON h.id = g.id_horario
      LEFT JOIN clase_iniciada ci ON g.id = ci.id_grupo AND ci.fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND  CONCAT(CURDATE(), " 23:25:59")
      LEFT JOIN usuarios u ON u.id = ci.id_teacher
      WHERE g.id_ciclo = ? AND ${ dia } = 1 AND hora_inicio = ?;`,[ actual, hora_inicio ],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      console.log(res)
      resolve(res)
    });
  });
};


Reportes.reporteHoraInicioFast = (ciclo,result)=>{
  sqlFAST.query(`SELECT DISTINCT hora_inicio FROM grupos g
    LEFT JOIN horarios h ON h.id = g.id_horario 
    WHERE g.id_ciclo = ? ORDER BY hora_inicio;`,[ciclo],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
}

Reportes.reporteHoraInicioInbi = (ciclo,result)=>{
  sqlINBI.query(`SELECT DISTINCT hora_inicio FROM grupos g
    LEFT JOIN horarios h ON h.id = g.id_horario 
    WHERE g.id_ciclo = ? ORDER BY hora_inicio;`,[ciclo],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
  })
}
module.exports = Reportes;

