var fs = require('fs');
const Usuarios = require("../models/usuario.models.js");

// Iniciar sesion
exports.session = (req, res)=>{
  Usuarios.login(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar el usuario" 
        });
      }
    } else 
    console.log('data',data)
    res.send(data);
  });
};

exports.getUsuarios = (req, res) => {
    Usuarios.getUsuarios((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los Usuarios"
            });
        else res.send(data);
    });
};


exports.getUsuariosALL = async (req, res) => {
  try{
    // Cargamos todos las preguntas no borradas
    let getUsuario = await Usuarios.getUsuariosALL( req.params.id_usuario ).then(response=> response)
    // Validamos que todo este bien y que si haya un usuarios
    if(getUsuario){
      const getPlanteles = await Usuarios.getPlantelesUsuario( req.params.id_usuario ).then(response=> response)
      getUsuario['planteles']   = getPlanteles
      getUsuario['idplanteles'] = getPlanteles.map( (registro) => { return registro.idplantel })
      res.status(200).send(getUsuario)
    }else{
      res.status(500).send({message:'usuario no existe'})
    }

  }catch( error ){
    res.status( 500 ).send({message: error ? error.message : 'Error en el servidor' })
  }

};



/*exports.putUsuarios = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }
    Usuarios.putUsuarios(req.params.idUsuarios_usuario, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre la subserie con el id ${req.params.idUsuarios_usuario }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar la subserie con el id" + req.params.idUsuarios_usuario
                });
            }
        } else res.send(data);
    });
}*/