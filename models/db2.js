const mysql    = require('mysql');
const dbConfig = require("../config/db2.config.js");

// Create a connection to the database
const connection2 = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  dateStrings: true
});

// open the MySQL connection
connection2.connect(error => {
  if (error) throw error;
  console.log("|********* CONECCION A" +" "+ dbConfig.DB  +" "+ "CORRECTAMENTE **********|");
});

module.exports = connection2;