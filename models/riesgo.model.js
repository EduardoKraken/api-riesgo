const sql = require("./db.js");   // FAST
const sql2 = require("./db2.js"); // INBI
const sql3 = require("./db3.js"); // ENGLISHADMIN
/*************************************************************************/
/******************************* F A S T *********************************/
/*************************************************************************/

// constructor
const Riesgo = function (riesgo) {
    this.idweb = riesgo.idweb;
    this.nomcli = riesgo.nomcli;
    this.calle = riesgo.calle;
};

Riesgo.getAlumnosRiesgoFast = (c, result) => {
  var ciclo = c
  sql.query(`SELECT g.nombre as grupo, CONCAT(u.nombre," ",u.apellido_paterno," ", IFNULL(u.apellido_materno,"")) as nombre, u.id, g.id as id_grupo, g.id_curso, g.id_nivel, g.codigo_acceso, g.id_plantel,u.iderp,
            (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy, u.usuario AS matricula,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia
            FROM grupo_alumnos ga
						LEFT JOIN grupos g ON g.id = ga.id_grupo
						LEFT JOIN usuarios u ON u.id = ga.id_alumno
						WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%INDUC%';`, [c,c,c], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    // No hubo error, si trajo a los alumnos
    var paylod = []
  	// Consultamos las asistencias de los alumnos
    sql.query(`SELECT a.valor_asistencia, a.id_usuario, a.fecha_asistencia
     FROM asistencia_grupo a
      LEFT JOIN grupos g ON a.id_grupo = g.id 
      WHERE g.id_ciclo = ?
      ORDER BY a.id_usuario;`,[ciclo],(err, asistencias) => {

      sql.query(`SELECT ce.id_alumno, ce.id_grupo, COUNT(*) as contestado FROM control_grupos_calificaciones_evaluaciones ce 
        LEFT JOIN grupo_alumnos ga ON ce.id_grupo = ga.id_grupo
        LEFT JOIN grupos g ON g.id = ga.id_grupo 
        LEFT JOIN evaluaciones e ON e.id = ce.id_evaluacion
        WHERE g.id_ciclo = ? AND e.tipo_evaluacion = 1 AND e.idCategoriaEvaluacion = 1  AND ce.id_grupo = g.id AND ce.id_alumno = ga.id_alumno
        GROUP BY ce.id_alumno, ce.id_grupo;`,[ciclo],(err, ejercicios) => {

        sql.query(`SELECT cc.calificacion, cc.id_alumno as id FROM control_grupos_calificaciones_evaluaciones cc 
          LEFT JOIN grupos g ON g.id = cc.id_grupo WHERE g.id_ciclo = ? AND 
          cc.id_evaluacion IN (SELECT id FROM evaluaciones e WHERE e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 1 OR e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 2);`,[ciclo],(err, examenes) => {

          var inicio = new Date(res[0].inicio); //Fecha inicial
          var fin = new Date(res[0].hoy); //Fecha final
          var timeDiff = Math.abs(fin.getTime() - inicio.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas
          var cuentaDom = 0; //Número de  Domingos
          var cuentaSab = 0; //Número de Sábados 
          var cuentaLMV = 0; //Número de lunes a viernes
          var cuentaLV = 0; //Número de lunes a viernes
          var cuentaMJ = 0; //Número de martes y jueves
          var array = new Array(diffDays);

          // Cantidad de días que han transcurrido entre 
          for (var i=0; i < diffDays; i++){
              //0 => Domingo
              if (inicio.getDay() == 0) {
                  cuentaDom++;
              }
              //0 => Domingo - 6 => Sábado
              if (inicio.getDay() == 6) {
                  cuentaSab++;
              }
              if (inicio.getDay() == 1 || inicio.getDay() == 3 || inicio.getDay() == 5) {
                  cuentaLMV++;
              }
              if (inicio.getDay() == 2 || inicio.getDay() == 4) {
                  cuentaMJ++;
              }

              if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4 || inicio.getDay() == 5) {
                  cuentaLV++;
              }
              inicio.setDate(inicio.getDate() + 1);
          }

  		    if (err) {
  		      console.log("error: ", err);
  		      result(null, err);
  		      return;
  		    }

          for(const i in res){
    		    var riesgo = false
    		    var countFalta = 0
    		    var countAsistencia = 0
            var asistencias_totales = 0
            var faltas_totales = 0

            var arrayFalta = []
    		    // No hubo error, si trajo a los alumnos
    		    // Hacemos un ciclo para ver si tiene 2 asistencias seguidas y mandarlo al panel de riesgo
    		    for(const j in asistencias){
              // Comparamos que sea el mismo usuario
              if(asistencias[j].id_usuario == res[i].id){

      		    	if(asistencias[j].valor_asistencia == 2){
                  arrayFalta.push(asistencias[j].fecha_asistencia)
      		    		countFalta += 1
                  faltas_totales += 1
      		    	}else{
                  if(asistencias[j].valor_asistencia > 0 && asistencias[j].valor_asistencia < 4){
                    asistencias_totales += 1
      		    		  countFalta = 0
                  }
      		    	}

              }

    		    }

            // Ciclo para ejercicios
            var estatus_ejercicios = 'NULO'
            var diferencia = 0
            // For para ejercicios
            for(const k in ejercicios){
              // Comparamos que sea el mismo usuario
              if(ejercicios[k].id_alumno == res[i].id){
                // Sacar el curso Lunes a viernes
                switch(res[i].id_curso){
                  case 2: // Lunes a viernes
                    diferencia = cuentaLV - ejercicios[k].contestado
                    if(res[0].diferencia >=  3){
                      if(res[0].diferencia >=  15){
                        if(diferencia > 3){estatus_ejercicios='ALTO'}else{estatus_ejercicios ='NULO'}
                      }else{
                        if(diferencia > 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios ='NULO'}
                      }
                    }else{
                      estatus_ejercicios='NULO'
                    }
                      
                  break;

                  case 4: //Lunes miercoles viernes
                    diferencia = cuentaLMV - ejercicios[k].contestado
                    if(diferencia > 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                  break;

                  case 6: //Sabatino
                    if(res[0].diferencia >= 6){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 7: //Dominical
                    if(res[0].diferencia >= 7){
                      diferencia = cuentaDom - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 8: //Martes y jueves
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaMJ - ejercicios[k].contestado
                      if(diferencia > 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;
                }

              }
            }

            const found = ejercicios.find( id => id.id_alumno == res[i].id  );
            if(!found){
              estatus_ejercicios='ALTO'
            }

            var estatus = 'NULO'
            switch(res[i].id_curso){
              case 2: // Lunes a viernes
                if(res[0].diferencia >= 2){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 4: //Lunes miercoles viernes
                if(res[0].diferencia >= 2){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 6: //Sabatino
                if(res[0].diferencia >= 6){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 7: //Dominical
                if(res[0].diferencia >= 7){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 8: //Martes y jueves
                if(res[0].diferencia >= 4){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;
            }// fin del switch

            // Ciclo para examenes
            var count = 0
            var estatus_examenes  = 'NULO'
            
            if(res[0].diferencia >= 15){
              var existeExamen = examenes.find(exa => exa.id == res[i].id && exa.calificacion > 0)
              if(existeExamen){
                estatus_examenes = 'NULO'
              }else{
                estatus_examenes = 'ALTO'
              }
            }


    		    // validamos el estatus para dos faltas seguidas
    		    // Lunes a viernes
            paylod.push({
              matricula:              res[i].matricula,
              id_alumno:              res[i].id,
              grupo:                  res[i].grupo,
              nombre:                 res[i].nombre,
              curso:                  res[i].id_curso,
              nivel:                  res[i].id_nivel,
              estatus:                estatus,
              estatus_ejercicios:     estatus_ejercicios,
              diferencia:             diferencia,
              estatus_examenes:       estatus_examenes,
              estatus_calificaciones: 'NULO',
              estatus_registro:       'NULO',
              id_grupo:               res[i].id_grupo,
              id_plantel:             res[i].id_plantel,
              faltas:                 arrayFalta,
              iderp:                  res[i].iderp,
              monto:                  'NI',
            })

          }
          result(null, paylod);
  		  });
      })
    })
  });
};

// Riesgo.getAlumnosRiesgoFastCalif = (ciclo, result) => {
//   sql.query(`SELECT DISTINCT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, c.id_alumno, cu.valor_asistencia,g.id_curso,
//     cu.valor_ejercicios, cu.valor_examenes, c.id_grupo,c.calificacion, cu.total_dias_laborales, e.id as idevaluacion,
//     cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
//     (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 1) as asistencia,
//     (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 2) as falta,
//     (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 3) as retardo,
//     (SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 4) as justicada
//     FROM control_grupos_calificaciones_evaluaciones c
//     LEFT JOIN grupos g ON g.id = c.id_grupo
//     LEFT JOIN cursos cu ON cu.id = g.id_curso
//     LEFT JOIN evaluaciones e ON e.id = id_evaluacion
//     LEFT JOIN usuarios u ON u.id = c.id_alumno WHERE g.id_ciclo = ? ORDER BY nombre;`, [ciclo], (err, res) => {
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//       return;
//     }
//     result(null, res);
//   })
// };

Riesgo.getCiclosFast = (result)=>{
  sql.query(`SELECT * FROM ciclos WHERE deleted = 0`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Riesgo.getAlumnosRiesgoPagoFast = (iderp, result)=>{
  sql3.query(`SELECT id_alumno, pagado AS total_pagado FROM alumnos_grupos_especiales_carga WHERE id_ciclo = ?;`,[iderp],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Grupos por teacher
Riesgo.getGruposTeacherFast = (data,result)=>{
  sql.query(`SELECT g.id  FROM grupo_teachers gt
    LEFT JOIN grupos g ON g.id = gt.id_grupo
    LEFT JOIN usuarios u ON u.id = gt.id_teacher OR u.id = gt.id_teacher_2
    WHERE g.id_ciclo = ? AND u.email  = ? ;`,
    [data.ciclo,data.email],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


// Grupos por teacher
Riesgo.getRecursoFaltaFast = (data,result)=>{
  sql.query(`SELECT r.nombre, r.id_leccion, (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_ciclo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) as calificacion
FROM recursos r WHERE r.id_nivel = ? AND r.id_curso = ? AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0;`,
    [data.ciclo,data.id_alumno,data.nivel, data.curso],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

/*************************************************************************/
/******************************** I N B I ********************************/
/*************************************************************************/

Riesgo.getAlumnosRiesgoInbi = (c, result) => {
  var ciclo = c
  sql2.query(`SELECT g.nombre as grupo, CONCAT(u.nombre," ",u.apellido_paterno," ", IFNULL(u.apellido_materno,"")) as nombre, u.id, g.id as id_grupo, g.id_curso, g.id_nivel, g.codigo_acceso, g.id_plantel,u.iderp,
            (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy, u.usuario AS matricula,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia
            FROM grupo_alumnos ga
            LEFT JOIN grupos g ON g.id = ga.id_grupo
            LEFT JOIN usuarios u ON u.id = ga.id_alumno
            WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%INDUC%';`, [c,c,c], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    // No hubo error, si trajo a los alumnos
    var paylod = []
    // for(const i in res){
    // Consultamos las asistencias de los alumnos
    // sql.query(`SELECT valor_asistencia FROM asistencia_grupo WHERE id_grupo = ? AND id_usuario = ?;`, [res[i].id_grupo, res[i].id], (err, asistencias) => {
    sql2.query(`SELECT a.valor_asistencia, a.id_usuario, a.fecha_asistencia
     FROM asistencia_grupo a
      LEFT JOIN grupos g ON a.id_grupo = g.id 
      WHERE g.id_ciclo = ?
      ORDER BY a.id_usuario;`,[ciclo],(err, asistencias) => {

      sql2.query(`SELECT ce.id_alumno, ce.id_grupo, COUNT(*) as contestado FROM control_grupos_calificaciones_evaluaciones ce 
        LEFT JOIN grupo_alumnos ga ON ce.id_grupo = ga.id_grupo
        LEFT JOIN grupos g ON g.id = ga.id_grupo 
        LEFT JOIN evaluaciones e ON e.id = ce.id_evaluacion
        WHERE g.id_ciclo = ? AND e.tipo_evaluacion = 1 AND e.idCategoriaEvaluacion = 1  AND ce.id_grupo = g.id AND ce.id_alumno = ga.id_alumno
        GROUP BY ce.id_alumno, ce.id_grupo;`,[ciclo],(err, ejercicios) => {

        sql2.query(`SELECT u.id,
          IFNULL((SELECT calificacion FROM control_grupos_calificaciones_evaluaciones cc WHERE 
          cc.id_alumno = u.id AND cc.id_grupo = g.id AND
          cc.id_evaluacion IN(SELECT id FROM evaluaciones e WHERE e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 1 OR e.tipo_evaluacion = 2 AND e.idCategoriaEvaluacion = 2) LIMIT 1),0) As calificacion
          FROM grupo_alumnos ga
          LEFT JOIN usuarios u ON u.id = ga.id_alumno
          LEFT JOIN grupos g ON g.id = ga.id_grupo
          WHERE g.id_ciclo = ?;`,[ciclo],(err, examenes) => {

          var inicio = new Date(res[0].inicio); //Fecha inicial
          var fin = new Date(res[0].hoy); //Fecha final
          var timeDiff = Math.abs(fin.getTime() - inicio.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); //Días entre las dos fechas
          var cuentaLV = 0; //Número de lunes a viernes
          var cuentaSab = 0; //Número de Sábados 
          var cuentaLMV = 0; //Número de lunes a viernes
          var cuentaLJ = 0; //Número de martes y jueves
          var array = new Array(diffDays);

          // 2 Lunes a Viernes
          // 4 Lunes miercoles viernes
          // 9 Lunes a jueves
          // 3 Sabatino semi-intensivo a
          // 7 Sabatino
          // 8 Sabatino semi-intensivo b

          for (var i=0; i < diffDays; i++){
            //6 => Sábado
            if (inicio.getDay() == 6) {
                cuentaSab++;
            }
            // Lunes miercoles y viernes
            if (inicio.getDay() == 1 || inicio.getDay() == 3 || inicio.getDay() == 5) {
                cuentaLMV++;
            }
            // Lunes a juves
            if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4) {
                cuentaLJ++;
            }
            // Lunes a viernes
            if (inicio.getDay() == 1 || inicio.getDay() == 2 || inicio.getDay() == 3 || inicio.getDay() == 4 || inicio.getDay() == 5) {
                cuentaLV++;
            }
            inicio.setDate(inicio.getDate() + 1);
          }

          if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }

          for(const i in res){
            var riesgo = false
            var countFalta = 0
            var countAsistencia = 0
            var asistencias_totales = 0
            var faltas_totales = 0

            var arrayFalta = []
            // No hubo error, si trajo a los alumnos
            // Hacemos un ciclo para ver si tiene 2 asistencias seguidas y mandarlo al panel de riesgo
            for(const j in asistencias){
              // Comparamos que sea el mismo usuario
              if(asistencias[j].id_usuario == res[i].id){

                if(asistencias[j].valor_asistencia == 2){
                  arrayFalta.push(asistencias[j].fecha_asistencia)
                  countFalta += 1
                  faltas_totales += 1
                }else{
                  if(asistencias[j].valor_asistencia > 0 && asistencias[j].valor_asistencia < 4){
                    asistencias_totales += 1
                    countFalta = 0
                  }
                }

              }

            }


          // 2 Lunes a Viernes
          // 4 Lunes miercoles viernes
          // 9 Lunes a jueves
          // 3 Sabatino semi-intensivo a
          // 8 Sabatino semi-intensivo b
          // 7 Sabatino

            // Ciclo para ejercicios
            var estatus_ejercicios = 'NULO'
            var diferencia = 0
            // For para ejercicios
            for(const k in ejercicios){
              // Comparamos que sea el mismo usuario
              if(ejercicios[k].id_alumno == res[i].id){
                // Sacar el curso Lunes a viernes
                switch(res[i].id_curso){
                  case 2: // Lunes a viernes
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaLV - ejercicios[k].contestado
                      if(res[0].diferencia >= 15){
                        if(diferencia > 3){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                      }else{
                        if(diferencia > 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                      }
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 4: //Lunes miercoles viernes
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 9: //Lunes a jueves
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 2){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  // Todos los cursos del sabatino
                  case 3: //Sabatino
                    if(res[0].diferencia >= 6){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 7: //Sabatino
                    if(res[0].diferencia >= 6){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;

                  case 8: //Martes y jueves
                    if(res[0].diferencia >= 2){
                      diferencia = cuentaSab - ejercicios[k].contestado
                      if(diferencia >= 1){estatus_ejercicios='ALTO'}else{estatus_ejercicios='NULO'}
                    }else{
                      estatus_ejercicios='NULO'
                    }
                  break;
                }

              }
            }


            const found = ejercicios.find( id => id.id_alumno == res[i].id  );
            if(!found){
              estatus_ejercicios='ALTO'
            }

            // Ciclo para examenes
            var estatus_examenes  = 'NULO'


            if(res[0].diferencia >= 15){

              var existeExamen = examenes.find(exa => exa.id == res[i].id && exa.calificacion > 0)
              if(existeExamen){
                estatus_examenes = 'NULO'
              }else{
                estatus_examenes = 'ALTO'
              }
            }

            // Calular asistencias
            var estatus = 'NULO'
            switch(res[i].id_curso){
              case 2: // Lunes a viernes
                if(res[0].diferencia >= 3){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 4: //Lunes miercoles viernes
                if(res[0].diferencia >= 4){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 9: //Lunes a jueves
                if(res[0].diferencia >= 3){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              // Todos los cursos del sabatino
              case 3: //Sabatino
                if(res[0].diferencia >= 6){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 7: //Sabatino
                if(res[0].diferencia >= 6){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;

              case 8: //Martes y jueves
                if(res[0].diferencia >= 2){
                  // validamos los estatus del alumno para saber en que rango de asistencias esta, nulo, medio o alto
                  if(asistencias_totales == 0){
                    estatus = 'ALTO'
                  }else{
                    if(faltas_totales == 0){
                      estatus = 'NULO'
                    }else{estatus = 'MEDIO'}
                  }
                }else{
                  estatus = 'NULO'
                }
              break;
            }

            

            paylod.push({
              matricula:               res[i].matricula,
              id_alumno:               res[i].id,
              grupo:                   res[i].grupo,
              nombre:                  res[i].nombre,
              curso:                   res[i].id_curso,
              nivel:                   res[i].id_nivel,
              estatus:                 estatus,
              estatus_ejercicios:      estatus_ejercicios,
              diferencia:              diferencia,
              estatus_examenes:        estatus_examenes,
              estatus_calificaciones:  'NULO',
              estatus_registro:        'NULO',
              id_grupo:                res[i].id_grupo,
              id_plantel:              res[i].id_plantel,
              faltas:                  arrayFalta,
              iderp:                   res[i].iderp,
              monto:                   'NI',
            })

          }
          // console.log(paylod.length)
          result(null, paylod);
        });
      })
    })
    
  });
};

Riesgo.getCiclosInbi = (result)=>{
  sql2.query(`SELECT * FROM ciclos WHERE deleted = 0`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Grupos por teacher
Riesgo.getGruposTeacherInbi = (data,result)=>{
  sql2.query(`SELECT g.id  FROM grupo_teachers gt
    LEFT JOIN grupos g ON g.id = gt.id_grupo
    LEFT JOIN usuarios u ON u.id = gt.id_teacher OR u.id = gt.id_teacher_2
    WHERE g.id_ciclo = ? AND u.email  = ? ;`,
    [data.ciclo,data.email],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Grupos por teacher
Riesgo.getRecursoFaltaInbi = (data,result)=>{
  sql2.query(`SELECT r.nombre, r.id_leccion, (SELECT calificacion FROM control_grupos_calificaciones_evaluaciones WHERE  id_ciclo = ? AND id_alumno = ? AND id_evaluacion = r.id_evaluacion) as calificacion
FROM recursos r WHERE r.id_nivel = ? AND r.id_curso = ? AND r.id_tipo_evaluacion = 1 AND r.id_categoria_evaluacion = 1 AND r.deleted = 0;`,
    [data.ciclo,data.id_alumno,data.nivel, data.curso],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

module.exports = Riesgo;