const sql = require("./db.js");   // FAST
const sql2 = require("./db2.js"); // INBI
const sql3 = require("./db3.js"); // ENGLISHADMIN


// constructor
const Historial_tickets = function (historial_tickets) {
    this.idweb = historial_tickets.idweb;
    this.nomcli = historial_tickets.nomcli;
    this.calle = historial_tickets.calle;
};

Historial_tickets.addHistorial_ticket = (c, result) => {
  sql.query(`INSERT INTO historial_ticket(idticket, respuesta, motivo,respuestaauxi) VALUES(?, ?, ?,?)`, [c.idticket, c.respuesta, c.motivo,c.respuestaauxi], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    
    result(null, {
      id: res.insertId,
      ...c
    });
  });
};

Historial_tickets.getHistorial_tickets = (result) => {
  sql.query(`SELECT * FROM historial_ticket`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.getHistorial_tickets_usuario = (id, result) => {
  sql.query(`SELECT * FROM historial_ticket where id_usuario = ?`, [id], (err, res) => {
    if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
    }
    result(null, res);
  })
};

Historial_tickets.getHistorialEstatus = (id, result) => {
  sql.query(`SELECT * FROM historial_ticket where idticket = ?`, [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


Historial_tickets.getHistorialTicketRespuesta = (id, result) => {
  sql.query(`SELECT ht.idhistorial_ticket, ht.motivo, ht.respuestaauxi, ht.idticket, ht.respuesta FROM historial_ticket ht, ticket t WHERE ht.idticket = t.idticket AND ht.idticket = ?`, [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.putHistorialTicketRespuesta = (id, respuesta, idticket, result) => {
  sql.query(`UPDATE historial_ticket SET respuesta = ? WHERE idhistorial_ticket = ?`, [respuesta, id], (err, res) => {
    sql.query(`UPDATE ticket SET estatus = 5 WHERE idticket = ?`, [idticket], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      result(null, res);
    })
  })
}

Historial_tickets.putHistorial_tickets = (id, sub, result) => {
  sql.query(` UPDATE historial_ticket SET  respuestaauxi=?, respuesta=?, estatus=? WHERE idhistorial_ticket = ?`,
    [sub.respuestaauxi,sub.respuesta,sub.estatus,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({
            kind: "not_found"
        }, null);
        return;
      }

      result(null, {
        id: id,
        ...sub
      });
    }
  );
};

Historial_tickets.getCalificacionesAlumnos = (data,result) => {
  console.log('entre a fast',data.id_plantel)
  sql.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",u.apellido_materno) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,
cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales,
cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 1) as asistencia,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 2) as falta,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 3) as retardo,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 4) as justicada
 FROM control_grupos_calificaciones_evaluaciones c
LEFT JOIN grupos g ON g.id = c.id_grupo
LEFT JOIN cursos cu ON cu.id = g.id_curso
LEFT JOIN evaluaciones e ON e.id = id_evaluacion
LEFT JOIN usuarios u ON u.id = c.id_alumno
LEFT JOIN datos_alumno da ON da.id_usuario = u.id
            WHERE  g.id_plantel = ? AND g.id_ciclo = ? AND c.deleted = 0 ORDER BY nombre;`,[data.id_plantel, data.ciclo],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.getCalificacionesAlumnosInbi = (data,result) => {
  sql2.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",u.apellido_materno) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,
cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales,
cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 1) as asistencia,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 2) as falta,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 3) as retardo,
(SELECT COUNT(*) as valor1 FROM asistencia_grupo WHERE id_usuario = u.id AND id_grupo = c.id_grupo AND valor_asistencia = 4) as justicada
 FROM control_grupos_calificaciones_evaluaciones c
LEFT JOIN grupos g ON g.id = c.id_grupo
LEFT JOIN cursos cu ON cu.id = g.id_curso
LEFT JOIN evaluaciones e ON e.id = id_evaluacion
LEFT JOIN usuarios u ON u.id = c.id_alumno
LEFT JOIN datos_alumno da ON da.id_usuario = u.id
            WHERE  g.id_plantel = ? AND g.id_ciclo = ? AND c.deleted = 0 ORDER BY nombre;`,[data.id_plantel, data.ciclo],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


Historial_tickets.putHistorial = (id, sub, result) => {
  sql3.query(` UPDATE historial_asistencia SET  motivo_rechazo=? WHERE idhistorial_asistencia = ?`,
    [sub.motivo_rechazo,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({
            kind: "not_found"
        }, null);
        return;
      }

      // Actualizar la base de datos de inbi
      if(sub.unidad_negocio == 0){
        sql2.query(` UPDATE asistencia_grupo SET  estatus_llamada = 4 WHERE id = ?`,
          [sub.id_falta],
          (err, res) => {
            if (err) {
              console.log("error: ", err);
              result(null, err);
              return;
            }

            if (res.affectedRows == 0) {
              result({
                  kind: "not_found"
              }, null);
              return;
            }

            
            result(null, {
              id: id,
              ...sub
            });
          }
        );
      }else{
        sql.query(` UPDATE asistencia_grupo SET  estatus_llamada = 4 WHERE id = ?`,
          [sub.id_falta],
          (err, res) => {
            if (err) {
              console.log("error: ", err);
              result(null, err);
              return;
            }

            if (res.affectedRows == 0) {
              result({
                  kind: "not_found"
              }, null);
              return;
            }

            result(null, {
              id: id,
              ...sub
            });
          }
        );
      }
    }
  );
};

Historial_tickets.putHistorialAceptar = (id, sub, result) => {
  // Actualizar la base de datos de inbi
  if(sub.unidad_negocio == 0){
    sql2.query(` UPDATE asistencia_grupo SET  estatus_llamada = 2 WHERE id = ?`,
      [sub.id_falta],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }

        if (res.affectedRows == 0) {
          result({
              kind: "not_found"
          }, null);
          return;
        }
        result(null, {
          id: id,
          ...sub
        });
      }
    );
  }else{
    sql.query(` UPDATE asistencia_grupo SET  estatus_llamada = 2 WHERE id = ?`,
      [sub.id_falta],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }

        if (res.affectedRows == 0) {
          result({
              kind: "not_found"
          }, null);
          return;
        }

        result(null, {
          id: id,
          ...sub
        });
      }
    );
  }
}

Historial_tickets.getAlumnosFAST = ( ciclo ) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, u.id AS id_alumno, g.nombre as 'grupo', g.id_curso, g.id AS id_grupo,
    cu.valor_asistencia, cu.valor_ejercicios, cu.valor_examenes, cu.total_dias_laborales, cu.num_examenes, cu.num_ejercicios FROM grupo_alumnos ga 
    LEFT JOIN grupos g ON g.id = ga.id_grupo 
    LEFT JOIN usuarios u ON u.id = ga.id_alumno
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    WHERE g.id_ciclo = ${ciclo} AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%';`,(err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};


Historial_tickets.getRiesgoAlumnosFast = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT DISTINCT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,g.id_curso,g.id_plantel,
    cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales, e.id as idevaluacion, u.usuario AS matricula,
    (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia,
    cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
    cc.calificacion_final_primera_oportunidad, cc.calificacion_final_segunda_oportunidad
    FROM control_grupos_calificaciones_evaluaciones c
    LEFT JOIN grupos g ON g.id = c.id_grupo
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    LEFT JOIN evaluaciones e ON e.id = id_evaluacion
    LEFT JOIN datos_alumno da ON da.id_usuario = c.id_alumno
    LEFT JOIN usuarios u ON u.id = c.id_alumno
    LEFT JOIN cardex_curso cc ON cc.id_alumno = u.id AND cc.id_grupo = g.id
    WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%' AND c.deleted = 0 ORDER BY nombre;`,[idCiclo,idCiclo,idCiclo],(err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Historial_tickets.asistenciasFast = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT a.* FROM asistencia_grupo a 
      LEFT JOIN grupos g ON g.id = a.id_grupo
      WHERE g.id_ciclo = ? ;`,[idCiclo],(err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};


Historial_tickets.getRiesgoAlumnosInbi = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sql2.query(`SELECT DISTINCT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, c.id_alumno, cu.valor_asistencia, da.telefono, da.celular,g.id_curso,g.id_plantel,
    cu.valor_ejercicios, cu.valor_examenes, c.id_grupo, g.nombre as 'grupo',c.calificacion, cu.total_dias_laborales, e.id as idevaluacion, u.usuario AS matricula,
    (SELECT fecha_inicio FROM ciclos WHERE id = ?) as inicio, NOW() as hoy,
            DATEDIFF(NOW(),(SELECT fecha_inicio FROM ciclos WHERE id = ?)) as diferencia,
    cu.num_examenes, cu.num_ejercicios, e.tipo_evaluacion, e.idCategoriaEvaluacion,
    cc.calificacion_final_primera_oportunidad, cc.calificacion_final_segunda_oportunidad
    FROM control_grupos_calificaciones_evaluaciones c
    LEFT JOIN grupos g ON g.id = c.id_grupo
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    LEFT JOIN evaluaciones e ON e.id = id_evaluacion
    LEFT JOIN datos_alumno da ON da.id_usuario = c.id_alumno
    LEFT JOIN usuarios u ON u.id = c.id_alumno
    LEFT JOIN cardex_curso cc ON cc.id_alumno = u.id AND cc.id_grupo = g.id
    WHERE g.id_ciclo = ? AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%indu%' AND c.deleted = 0 ORDER BY nombre;`,[idCiclo,idCiclo,idCiclo],(err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};


Historial_tickets.getAlumnosINBI = ( ciclo ) => {
   return new Promise((resolve, reject) => {
    sql2.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, u.id AS id_alumno, g.nombre as 'grupo', g.id_curso, g.id AS id_grupo,
    cu.valor_asistencia, cu.valor_ejercicios, cu.valor_examenes, cu.total_dias_laborales, cu.num_examenes, cu.num_ejercicios FROM grupo_alumnos ga 
    LEFT JOIN grupos g ON g.id = ga.id_grupo 
    LEFT JOIN usuarios u ON u.id = ga.id_alumno
    LEFT JOIN cursos cu ON cu.id = g.id_curso
    WHERE g.id_ciclo = ${ciclo} AND g.nombre NOT LIKE '%certi%' AND g.nombre NOT LIKE '%induc%'`,(err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};


Historial_tickets.asistenciasInbi = (idCiclo) => {
   return new Promise((resolve, reject) => {
    sql2.query(`SELECT a.* FROM asistencia_grupo a 
      LEFT JOIN grupos g ON g.id = a.id_grupo
      WHERE g.id_ciclo = ? ;`,[idCiclo],(err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};





Historial_tickets.getRiesgoAlumnoRegistro = (result) => {
  sql.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, g.nombre AS grupo, u.id, g.id as id_grupo FROM usuarios u 
      LEFT JOIN datos_alumno d ON d.id_usuario = u.id
      LEFT JOIN grupo_alumnos ga ON ga.id_alumno =  u.id
      LEFT JOIN grupos g ON g.id = ga.id_grupo
      WHERE u.id_tipo_usuario = 1 AND g.nombre IS NOT NULL AND d.id_usuario IS NULL AND g.id_ciclo = 17;`,(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.getRiesgoAlumnoInbiRegistro = (result) => {
  sql2.query(`SELECT CONCAT(u.nombre," ",u.apellido_paterno," ",IFNULL(u.apellido_materno,"")) as nombre, g.nombre AS grupo, u.id, g.id as id_grupo FROM usuarios u 
LEFT JOIN datos_alumno d ON d.id_usuario = u.id
LEFT JOIN grupo_alumnos ga ON ga.id_alumno =  u.id
LEFT JOIN grupos g ON g.id = ga.id_grupo
WHERE u.id_tipo_usuario = 1 AND g.nombre IS NOT NULL AND d.id_usuario IS NULL AND g.id_ciclo = 16;`,(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Historial_tickets.getAsistenciaAlumnoGrupo = (data,result) => {
  console.log(data)
  sql.query(`SELECT id_usuario,valor_asistencia, COUNT(*) as total FROM asistencia_grupo WHERE id_usuario = ? AND id_grupo = ? 
    GROUP BY valor_asistencia;`,[data.id_alumno, data.id_grupo],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

module.exports = Historial_tickets;