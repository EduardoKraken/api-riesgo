const { result } = require("lodash");
const sqlERP = require("../db3.js");


//const constructor
const Kpi = function(depas) {};

Kpi.kpiInscripcionesGeneralFAST = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel, ciclo FROM gruposalumnos ga
    LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
    LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
    LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
    LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
    WHERE g.grupo LIKE '%CICLO%'
    AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
    LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
    WHERE g.grupo LIKE '%CICLO%'
    AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
     result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesGeneralINBI = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel, ciclo FROM gruposalumnos ga
    LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
    LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
    LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
    LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
    WHERE g.grupo LIKE '%CICLO%'
    AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
    LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
    WHERE g.grupo LIKE '%CICLO%'
    AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/***/

Kpi.kpiInscripcionesAlumnosCicloRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesAlumnosCicloRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/***/

Kpi.kpiInscripcionesAlumnosCicloDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesAlumnosCicloDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/***/

Kpi.kpiInscripcionesAlumnosRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechafin,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesAlumnosRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechafin,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/***/

Kpi.kpiInscripcionesAlumnosDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesAlumnosDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo AS vendedora, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

Kpi.kpiInscripcionesTotalFAST = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesTotalINBI = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesCicloDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesCicloDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesCicloRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesCicloRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, g.id_plantel, p.plantel, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY plantel;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

// Obtener todos los planteles
// Obtener todos los planteles
Kpi.getPlanteles = result => {
  sqlERP.query(`SELECT * FROM planteles  WHERE activo_sn = 1`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("planteles: ", res);
    result(null, res);
  });
};

// Obtener todos los ciclos
Kpi.getCiclos = result => {
  sqlERP.query(`SELECT * FROM ciclos  WHERE activo_sn = 1`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("ciclos: ", res);
    result(null, res);
  });
};



Kpi.getCantActual = (id,idfast,result) => {
  sqlERP.query(`SELECT DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
WHERE a.id_ciclo = ?  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
OR a.id_ciclo = ? AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' 
AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("planteles: ", res);
    result(null, res);
  });
};

// Obtener todos los ciclos
Kpi.getCantSiguiente = (id,idfast,result) => {
  sqlERP.query(`SELECT  DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
  LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
  LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
  LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
  WHERE a.id_ciclo = ?  AND a.adeudo = 0  OR 
  a.id_ciclo = ?  AND a.adeudo = 0  GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("ciclos: ", res);
    result(null, res);
  });
};

Kpi.getCantSiguienteAvance = (id,idfast,result) => {
  sqlERP.query(`SELECT  DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
  LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
  LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
  LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
  WHERE a.id_ciclo = ?  AND a.adeudo = 0  AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 01:00:00") AND CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 23:59:59") OR 
  a.id_ciclo = ?  AND a.adeudo = 0 AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 01:00:00") AND CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 23:59:59") GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("ciclos: ", res);
    result(null, res);
  });
};

// TEEEEEEEEEEEEEEEEENNNNNNNNNNNNNNNSSSSSSSSSSSSS
Kpi.getCantActualTeens = (id,idfast,result) => {
  sqlERP.query(`SELECT DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
    LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    WHERE a.id_ciclo = ?  AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo LIKE '%TEENS%'
    AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
    OR a.id_ciclo = ? AND a.adeudo = 0  AND g.grupo NOT LIKE '%INVER%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo LIKE '%TEENS%'
    AND g.id_grupo IN (SELECT id_grupo FROM maestrosgrupo)
    GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("planteles: ", res);
    result(null, res);
  });
};

// Obtener todos los ciclos
Kpi.getCantSiguienteTeens = (id,idfast,result) => {
  sqlERP.query(`SELECT  DISTINCT a.*,p.plantel FROM alumnos_grupos_especiales_carga a 
    LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
    LEFT JOIN grupos g ON g.id_grupo = a.id_grupo
    LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
    WHERE a.id_ciclo = ?  AND a.adeudo = 0 AND g.grupo LIKE '%TEENS%' OR 
    a.id_ciclo = ?  AND a.adeudo = 0 AND g.grupo LIKE '%TEENS%' GROUP BY a.id_alumno;`,[id,idfast], (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    console.log("ciclos: ", res);
    result(null, res);
  });
};

/*****************************************************************************************************/
Kpi.kpiInscripcionesVendedoraCicloFAST = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraCicloINBI = (actual,result) => {
  sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario;`,[actual,actual], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};


Kpi.kpiInscripcionesVendedoraCicloRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT u.nombre_completo, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraCicloRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT u.nombre_completo, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 00:00:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario;`,[data.ciclo,data.fechaini,data.fechafin,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraCicloDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT u.nombre_completo, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraCicloDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT u.nombre_completo, count(DISTINCT ga.id_alumno) AS total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario;`,[data.ciclo,data.fechaini,data.fechaini,data.ciclo], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraRangoFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechafin,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraRangoINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechafin,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraDiaFAST = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};

Kpi.kpiInscripcionesVendedoraDiaINBI = (data,result) => {
  sqlERP.query(`SELECT DISTINCT ga.id_alumno, CONCAT(a.nombre, " ", a.apellido_paterno, " ", IFNULL(a.apellido_materno,"")) AS alumno, u.nombre_completo, plantel,ciclo FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      LEFT JOIN ciclos c ON c.id_ciclo = g.id_ciclo
      LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%FE%' AND ga.fecha_alta BETWEEN CONCAT(?," 00:00:00") AND CONCAT(?," 23:59:00")
      AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND ga.fecha_alta < CONCAT(?," 00:00:00")) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY alumno;`,[data.fechaini,data.fechaini,data.fechaini], (err, cicloActual) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, cicloActual);
  });
};



Kpi.TotalInscritosPorCicloFast = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
      SELECT  count(DISTINCT ga.id_alumno) as total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results[0].total)
      });
  })
}



Kpi.TotalInscritosPorCicloINBI = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
      SELECT  count(DISTINCT ga.id_alumno) as total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results[0].total)
      });
  })
}


Kpi.TotalInscritosPorCicloTEENS = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
      SELECT  count(DISTINCT ga.id_alumno) as total FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
      LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
      LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%'
      AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%'
      AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results[0].total)
      });
  })
}


Kpi.acumuladoPorSucursal = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1
        group by plantel  ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

Kpi.acumuladoPorSucursalTeens = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%'  AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'  AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 AND ga.fecha_alta <  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel  ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

/*********************************************************************************************/
/*********************************************************************************************/

Kpi.acumuladoPorSucursalAyer = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

Kpi.acumuladoPorSucursalAyerTeens = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as acumulado FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY acumulado;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

Kpi.semanaPorSucursal = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT  plantel, count(DISTINCT ga.id_alumno) as cantidad FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL weekday(CURDATE()) DAY), " 00:00:00") AND  CONCAT(ADDDATE(CURDATE(), INTERVAL (6 - weekday(CURDATE())) DAY), " 23:59:00")
        group by plantel ORDER BY cantidad;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}


Kpi.ayerPorSucursal = (ciclo) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`
        SELECT DISTINCT plantel, count(DISTINCT ga.id_alumno) as cantidad FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        group by plantel ORDER BY cantidad;
      `,
      [ciclo, ciclo],(err,results)=>{
        if(err){
          reject(err);
          return;
        }

        resolve(results)
      });
  })
}

/*****************************************************************************************************************/
Kpi.acumuladoPorVendedoraAyer = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' 
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' 
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){
        reject(err);
        return;
      }

      resolve(results)
    });
  });
};

Kpi.acumuladoPorVendedoraAyerTeens = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo NOT LIKE '%CERTI%' AND g.grupo NOT LIKE '%INVER%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 
        AND ga.fecha_alta BETWEEN CONCAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), " 00:00:00") AND  CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), " 23:59:00")
        GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){
        reject(err);
        return;
      }

      resolve(results)
    });
  });
};

Kpi.kpiInscripcionesVendedoraCiclo = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%'
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){
        reject(err);
        return;
      }

      resolve(results)
    });
  });
};


Kpi.kpiInscripcionesVendedoraCicloTEENS = (actual) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT DISTINCT count(DISTINCT ga.id_alumno) AS total, u.nombre_completo FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        LEFT JOIN planteles p ON p.id_plantel = g.id_plantel
        LEFT JOIN alumnos a ON a.id_alumno = ga.id_alumno
        LEFT JOIN usuarios u ON u.id_usuario = ga.id_usuario_ultimo_cambio
        LEFT JOIN alumnos_grupos_especiales_carga aa ON aa.id_alumno = ga.id_alumno
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo = ? AND ga.id_alumno NOT IN (SELECT ga.id_alumno FROM gruposalumnos ga
        LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
        WHERE g.grupo LIKE '%CICLO%' AND g.grupo LIKE '%TEENS%' 
        AND g.id_ciclo < ?) AND aa.adeudo = 0 AND aa.pagado > 1 GROUP BY u.id_usuario ORDER BY total;`,[actual,actual], (err, results) => {
      if(err){
        reject(err);
        return;
      }

      resolve(results)
    });
  });
};

/*****************************************************************************************************/
// RI por grupos

Kpi.getGruposActual = (inbi,fast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY g.id_grupo;`,[inbi,fast], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Kpi.alumosCicloActual = (inbi,fast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY a.id_alumno;`,[inbi,fast], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

Kpi.alumosCicloSiguiente = (inbi,fast,sigInbi,sigFast) => {
   return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT  DISTINCT a.id_alumno, g.grupo, g.id_grupo FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      AND a.id_alumno IN (SELECT  DISTINCT a.id_alumno FROM alumnos_grupos_especiales_carga a 
      LEFT JOIN gruposalumnos ga ON ga.id_alumno = a.id_alumno
      LEFT JOIN grupos g ON g.id_grupo = ga.id_grupo
      WHERE g.id_ciclo IN (?,?) 
      AND a.adeudo = 0 
      AND g.activo_sn = 1
      AND g.grupo NOT LIKE '%IND%' AND g.grupo NOT LIKE '%CERT%'
      GROUP BY g.id_grupo)
      GROUP BY g.id_grupo;`,[inbi,fast,sigInbi,sigFast], (err, res) => {
      if(err){
        reject(err);
        return;
      }
      console.log(res.length)
      resolve(res)
    });
  });
};





module.exports = Kpi;

