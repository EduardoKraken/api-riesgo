module.exports = app => {
  const usuarios = require('../controllers/usuarios.controllers') // --> ADDED THIS
  app.get("/usuarios.all", usuarios.getUsuarios);
  app.post("/sessions"   , usuarios.session); // iniciar sesion
  app.get("/tickets.usuarios.getAll/:id_usuario", usuarios.getUsuariosALL);
};