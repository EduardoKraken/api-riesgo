module.exports = app => {
  const riesgo = require('../controllers/riesgo.controllers') // --> ADDED THIS
  const historial_tickets = require('../controllers/historial_ticket.controllers') // --> ADDED THIS

  
  app.post("/riesgo.alumnos.fast",             riesgo.getAlumnosRiesgoFast);
  app.post("/riesgo.alumnos.pago", 				     riesgo.getAlumnosRiesgoPagoFast);
  app.post("/riesgo.calificaciones",           historial_tickets.getRiesgoAlumno);
  app.get("/riesgo.ciclos.fast",               riesgo.getCiclosFast);

  app.post("/riesgo.alumnos.inbi.inbi",        riesgo.getAlumnosRiesgoInbi);
  app.post("/riesgo.calificaciones.inbi",      historial_tickets.getRiesgoAlumnoInbi);
  app.get("/riesgo.ciclos.inbi",               riesgo.getCiclosInbi);

  // Modo teacher
  app.post("/riesgo.grupos.teacher.fast",           riesgo.getGruposTeacherFast);
  app.post("/riesgo.grupos.teacher.inbi",           riesgo.getGruposTeacherInbi);


  // Modo RECEPCIONISTA EJERCICIOS FALTANTES
  app.post("/riesgo.ejercicios.fast",                  riesgo.getRecursoFaltaFast);
  app.post("/riesgo.ejercicios.inbi",                  riesgo.getRecursoFaltaInbi);
  // app.post("/riesgo.grupos.teacher.inbi",           riesgo.getGruposTeacherInbi);

  
};

