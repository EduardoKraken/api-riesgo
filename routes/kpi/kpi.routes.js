module.exports = app => {
    const kpi = require('../../controllers/kpi/kpi.controllers') 

    // 
    

    /******************************************************************************************/
    app.get("/kpi.inscripciones.general.fast/:actual"      , kpi.kpiInscripcionesGeneralFAST); 
    app.get("/kpi.inscripciones.general.inbi/:actual"      , kpi.kpiInscripcionesGeneralINBI);
    app.post("/kpi.inscripciones.alumnos.ciclo.rango.fast" , kpi.kpiInscripcionesAlumnosCicloRangoFAST);
    app.post("/kpi.inscripciones.alumnos.ciclo.rango.inbi" , kpi.kpiInscripcionesAlumnosCicloRangoINBI);
    app.post("/kpi.inscripciones.alumnos.ciclo.dia.fast"   , kpi.kpiInscripcionesAlumnosCicloDiaFAST);
    app.post("/kpi.inscripciones.alumnos.ciclo.dia.inbi"   , kpi.kpiInscripcionesAlumnosCicloDiaINBI);
    app.post("/kpi.inscripciones.alumnos.rango.fast"       , kpi.kpiInscripcionesAlumnosRangoFAST);
    app.post("/kpi.inscripciones.alumnos.rango.inbi"       , kpi.kpiInscripcionesAlumnosRangoINBI);
    app.post("/kpi.inscripciones.alumnos.dia.fast"         , kpi.kpiInscripcionesAlumnosDiaFAST);
    app.post("/kpi.inscripciones.alumnos.dia.inbi"         , kpi.kpiInscripcionesAlumnosDiaINBI);
    app.get("/kpi.inscripciones.total.fast/:actual"        , kpi.kpiInscripcionesTotalFAST);
    app.get("/kpi.inscripciones.total.inbi/:actual"        , kpi.kpiInscripcionesTotalINBI);

    app.get("/kpi.inscripciones.porciclo.fast/:ciclo"      , kpi.kpiInscripcionesPorCicloFAST)
    app.get("/kpi.inscripciones.porciclo.INBI/:ciclo"      , kpi.kpiInscripcionesPorCicloINBI)

    // 
    /******************************************************************************************/
    app.post("/kpi.inscripciones.ciclo.dia.fast"          , kpi.kpiInscripcionesCicloDiaFAST);
    app.post("/kpi.inscripciones.ciclo.dia.inbi"          , kpi.kpiInscripcionesCicloDiaINBI);

    app.post("/kpi.inscripciones.ciclo.rango.fast"        , kpi.kpiInscripcionesCicloRangoFAST);
    app.post("/kpi.inscripciones.ciclo.rango.inbi"        , kpi.kpiInscripcionesCicloRangoINBI);

    // planteles
    /*******************************************************************************************/
    app.get("/kpi.planteles.all",     kpi.getPlanteles);
    app.get("/kpi.ciclos.all",        kpi.getCiclos);
    
    // Apis p los datos de RIMONTOS
    app.get("/kpi.cantactual/:id/:idfast",           kpi.getCantActual);
    app.get("/kpi.cantsiguiente/:id/:idfast",        kpi.getCantSiguiente);
    app.get("/kpi.cantsiguiente.avance/:id/:idfast", kpi.getCantSiguienteAvance);

    // Teens
    app.get("/kpi.cantactual.teens/:id/:idfast",           kpi.getCantActualTeens);
    app.get("/kpi.cantsiguiente.teens/:id/:idfast",        kpi.getCantSiguienteTeens);

    // Vendedora
    /*******************************************************************************************/
    app.get("/kpi.inscripciones.vendedora.ciclo.fast/:actual"        , kpi.kpiInscripcionesVendedoraCicloFAST);
    app.get("/kpi.inscripciones.vendedora.ciclo.inbi/:actual"        , kpi.kpiInscripcionesVendedoraCicloINBI);
    app.post("/kpi.inscripciones.vendedora.ciclo.rango.fast"         , kpi.kpiInscripcionesVendedoraCicloRangoFAST);
    app.post("/kpi.inscripciones.vendedora.ciclo.rango.inbi"         , kpi.kpiInscripcionesVendedoraCicloRangoINBI);
    app.post("/kpi.inscripciones.vendedora.ciclo.dia.fast"           , kpi.kpiInscripcionesVendedoraCicloDiaFAST);
    app.post("/kpi.inscripciones.vendedora.ciclo.dia.inbi"           , kpi.kpiInscripcionesVendedoraCicloDiaINBI);
    app.post("/kpi.inscripciones.vendedora.rango.fast"               , kpi.kpiInscripcionesVendedoraRangoFAST);
    app.post("/kpi.inscripciones.vendedora.rango.inbi"               , kpi.kpiInscripcionesVendedoraRangoINBI);
    app.post("/kpi.inscripciones.vendedora.dia.fast"                 , kpi.kpiInscripcionesVendedoraDiaFAST);
    app.post("/kpi.inscripciones.vendedora.dia.inbi"                 , kpi.kpiInscripcionesVendedoraDiaINBI);

    // KPI general
    /*******************************************************************************************/
    // Ventas por sucursal
    app.get("/kpi.ni.porciclo.general.FAST/:ciclo"      , kpi.kpiNiPorCicloGeneralFAST)
    app.get("/kpi.ni.porciclo.general.INBI/:ciclo"      , kpi.kpiNiPorCicloGeneralINBI)
    app.get("/kpi.ni.porciclo.general.TEENS/:ciclo"     , kpi.kpiNiPorCicloGeneralTEENS)
    
    app.get("/kpi.ni.vendedora.ciclo.fast/:ciclo"       , kpi.kpiNiVendedoraCicloFAST);
    app.get("/kpi.ni.vendedora.ciclo.inbi/:ciclo"       , kpi.kpiNiVendedoraCicloFAST);
    app.get("/kpi.ni.vendedora.ciclo.teens/:ciclo"      , kpi.kpiNiVendedoraCicloTEENS);

    /**********************************************************************************************/
    // RI por grupos
    app.get("/kpi.ri.grupos/:inbi/:fast/:sigInbi/:sigFast"    , kpi.kpiRiGrupos);

};