const sql3 = require("./db3.js");

// constructor
const Tickets = function (usuarios) {
  this.idweb = usuarios.idweb;
  this.nomcli = usuarios.nomcli;
  this.calle = usuarios.calle;
};

Tickets.addTicket = (c, result) => {
  var date = new Date();
  sql3.query(`INSERT INTO ticket(id_usuario,id_unidad_negocio, motivo,estatus, tipousuario) VALUES(?,?,?,?,?)`, [c.id_usuario, c.id_unidad_negocio, c.motivo, c.estatus, c.tipousuario], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Ticket: ", {
      id: res.insertId,
      ...c
    });
    result(null, {
      id: res.insertId,
      ...c
    });
  });
};

module.exports = Tickets;