module.exports = app => {
    const reportes = require('../../controllers/kpi/reportes.controllers') 

    app.get("/reportes.teachers.asistencias.fast/:actual"      , reportes.reporteTeacherAsistenciaFast); 
    app.get("/reportes.teachers.asistencias.inbi/:actual"      , reportes.reporteTeacherAsistenciaInbi); 

    app.get("/reportes.asistencias.teacher.fast/:actual"       , reportes.reporteAsistenciaTeacherFast); 
    app.get("/reportes.asistencias.teacher.inbi/:actual"       , reportes.reporteAsistenciaTeacherInbi); 

    app.post("/reportes.claseiniciada.fast"                    , reportes.reporteClaseIniciadaFast);
    app.post("/reportes.claseiniciada.inbi"                    , reportes.reporteClaseIniciadaInbi);

    app.get("/reportes.horainicio.fast/:actual"                , reportes.reporteHoraInicioFast);
    app.get("/reportes.horainicio.inbi/:actual"                , reportes.reporteHoraInicioInbi);
};

