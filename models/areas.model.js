const sql = require("./db.js");
const sql2 = require("./db2.js");

// constructor
const Areas = function(clientes) {
  this.idweb     = clientes.idweb;
  this.nomcli    = clientes.nomcli;
  this.calle     = clientes.calle;
};

Areas.addArea = (c, result) => {
	sql.query(`INSERT INTO areas_ticket(area)VALUES(?)`,[c.area],(err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Area: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
	});
};

Areas.getAreas = (result)=>{
	sql.query(`SELECT * FROM areas_ticket`,(err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};

Areas.putAreas = (id, sub, result) => {
  sql.query(` UPDATE areas_ticket SET area=? WHERE idareas_ticket = ?`,
   [sub.area,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated sub: ", { id: id, ...sub });
      result(null, { id: id, ...sub });
    }
  );
};

module.exports = Areas;