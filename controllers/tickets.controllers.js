const Tickets = require("../models/ticket.models.js");

// Crear un Ticket
exports.addTicket = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }
  // Guardar el Ticket en la BD
  Tickets.addTicket(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

