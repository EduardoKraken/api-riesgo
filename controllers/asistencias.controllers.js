const Asistencias = require("../models/asistencias.model.js");

exports.getAsistencias = (req,res) => {
  Asistencias.getAsistencias((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getHistorial = (req,res) => {
  Asistencias.getHistorial((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};


exports.getAsistenciasPlantel = (req,res) => {
  Asistencias.getAsistenciasPlantel(req.params.idplantel,req.params.ciclo,(err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getAsistenciasPlantelINBI = (req,res) => {
  Asistencias.getAsistenciasPlantelINBI(req.params.idplantel,req.params.ciclo,(err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getAsistenciasFast = (req,res) => {
  Asistencias.getAsistenciasFast((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getAsistenciasInbi = (req,res) => {
  Asistencias.getAsistenciasInbi((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};


exports.putAsistencia = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Asistencias.putAsistencia(req.params.id, req.body ,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
        message: `No encontre la subserie con el id ${req.params.id }.`
        });
      } else {
        res.status(500).send({
        message: "Error al actualizar la subserie con el id" + req.params.id 
        });
      }
    } 
    else res.send(data);
  });
};

exports.putAsistenciaINBI = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Asistencias.putAsistenciaINBI(req.params.id, req.body ,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
        message: `No encontre la subserie con el id ${req.params.id }.`
        });
      } else {
        res.status(500).send({
        message: "Error al actualizar la subserie con el id" + req.params.id 
        });
      }
    } 
    else res.send(data);
  });
};


// Crear un cliente
exports.addHistorial = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Asistencias.addHistorial(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

// reportes ********************************

exports.getHistorialEscuela = (req,res) => {
  Asistencias.getHistorialEscuela((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getHistorialSucursal = (req,res) => {
  Asistencias.getHistorialSucursal((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getAllAsistencias = (req,res) => {
  Asistencias.getAllAsistencias((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};


// *************************************************************************
// *************************************************************************
               // solo traer los pendientes por revisar


exports.getAsistenciasFastEstatus = (req,res) => {
  Asistencias.getAsistenciasFastEstatus((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getAsistenciasInbiEstatus = (req,res) => {
  Asistencias.getAsistenciasInbiEstatus((err, data) => {
    if (err)
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los clientes"
    });
    else res.send(data);
  });
};

exports.getAsistenciaAlumno = (req,res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  Asistencias.getAsistenciaAlumno(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};


exports.getTeachers = (req,res) => {
  Asistencias.getTeachers(req.params.idciclo,(err,data) =>{
    if(err){
      res.status(500).send({
        message: err.message || "Se produjo un error desconocido"
      })
    }else{
      res.send(data)
    }
  })
}

exports.getTeachersInbi = (req,res) => {
  Asistencias.getTeachersInbi(req.params.idciclo,(err,data) =>{
    if(err){
      res.status(500).send({
        message: err.message || "Se produjo un error desconocido"
      })
    }else{
      res.send(data)
    }
  })
}