const sql         = require("./db3.js");
const sqlNUEVOERP = require("./dbNuevoerp.js");

// constructor
const Usuarios = function (usuarios) {
    this.idweb = usuarios.idweb;
    this.nomcli = usuarios.nomcli;
    this.calle = usuarios.calle;
};

Usuarios.login = (loger,result) =>{
  sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?"
    ,[loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res)
  });
};

Usuarios.getUsuarios = (result) => {
    sql.query(`SELECT id_usuario, nombre_completo FROM usuarios ORDER BY nombre_completo ASC`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("schoolxID: ", res);
        result(null, res);
    })
};

Usuarios.getUsuariosALL = ( id ) => {
  return new Promise ((resolve, reject) =>{
    sql.query(`SELECT u.*,p.id_perfil, p.perfil, c.servidor_sip, c.usuario_sip, c.password_sip, c.ext FROM usuarios u
      LEFT JOIN crm_cuentas_sip c ON c.id_usuario = u.id_usuario
      LEFT JOIN perfilesusuario pu ON pu.id_usuario = u.id_usuario
      LEFT JOIN perfiles p ON p.id_perfil = pu.id_perfil
      WHERE u.id_usuario = ?`, [id], (err, res) => {
      if (err) {
        return reject ({ message: err.sqlMessage })
      }
      resolve(res[0]);
    })
  })
};

Usuarios.getPlantelesUsuario = ( id ) => {
  return new Promise((resolve,reject)=>{
    sqlNUEVOERP.query(`SELECT * FROM planteles_usuario pl 
        LEFT JOIN usuarios u ON u.idusuario = pl.idusuario
        WHERE u.iderp = ?; `,[id],(err, res) => {
      if(err){
        reject(err);
        return;
      }
      resolve(res)
    });
  });
};

/*Usuarios.putUsuarios = (id, sub, result) => {
    sql.query(` UPDATE usuarios SET usuario=?, password=?, nombre_usuario=?, apellido_usuario=?, nombre_completo=?, id_plantel=?, email=?, telefono=?, comentarios=?, ajusta_pagos_sn=?, eliminable_sn=?, tipo_dashboard=?, login_actual_sn=?, fecha_ultimo_login=?, fecha_alta=?, activo_sn=?, fecha_baja=?, id_usuario_ultimo_cambio=?, fecha_ultimo_cambio=?, tipo_usuario=?, id_alumno=?, id_trabajador=? WHERE id_usuario = ?`,
        [sub.usuario, sub.folio, sub.id_usuario, sub.id_unidad_negocio, sub.id_encargado, sub.id_auxiliar, sub.fecha_apertura, sub.fecha_cierre, sub.id_area, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
                return;
            }

            console.log("updated sub: ", {
                id: id,
                ...sub
            });
            result(null, {
                id: id,
                ...sub
            });
        }
    );
};*/

module.exports = Usuarios;