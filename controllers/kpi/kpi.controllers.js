const Kpi = require("../../models/kpi/kpi.model.js");

exports.kpiInscripcionesGeneralFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesGeneralFAST(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.kpiInscripcionesGeneralINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesGeneralINBI(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};
/**/
exports.kpiInscripcionesAlumnosCicloRangoFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesAlumnosCicloRangoFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesAlumnosCicloRangoINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesAlumnosCicloRangoINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

/**/
exports.kpiInscripcionesAlumnosCicloDiaFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesAlumnosCicloDiaFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesAlumnosCicloDiaINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesAlumnosCicloDiaINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};

/***/

exports.kpiInscripcionesAlumnosRangoFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosRangoFAST(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.kpiInscripcionesAlumnosRangoINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosRangoINBI(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

/***/

exports.kpiInscripcionesAlumnosDiaFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosDiaFAST(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.kpiInscripcionesAlumnosDiaINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesAlumnosDiaINBI(req.body,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};




/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

exports.kpiInscripcionesTotalFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesTotalFAST(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.kpiInscripcionesTotalINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesTotalINBI(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.kpiInscripcionesCicloDiaFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloDiaFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesCicloDiaINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloDiaINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};



exports.kpiInscripcionesCicloRangoFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloRangoFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesCicloRangoINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesCicloRangoINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.getPlanteles = (req, res) => {
  Kpi.getPlanteles((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCiclos = (req, res) => {
  Kpi.getCiclos((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los planteles activos
exports.getCantActual = (req, res) => {
  Kpi.getCantActual(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCantSiguiente = (req, res) => {
  Kpi.getCantSiguiente(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};


// Obtener todos los ciclos  activos
exports.getCantSiguienteAvance = (req, res) => {
  Kpi.getCantSiguienteAvance(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// TEEEEEENS

// Obtener todos los planteles activos
exports.getCantActualTeens = (req, res) => {
  Kpi.getCantActualTeens(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

// Obtener todos los ciclos  activos
exports.getCantSiguienteTeens = (req, res) => {
  Kpi.getCantSiguienteTeens(req.params.id,req.params.idfast, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar las calificaciones"
      });
    else res.send(data);
  });
};

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/
exports.kpiInscripcionesVendedoraCicloFAST = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesVendedoraCicloFAST(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};


exports.kpiInscripcionesVendedoraCicloINBI = (req, res) => {
  //mandamos a llamar al model y le enviamos dos parametros err=para errores 
  //y data= los datos que estamos recibiendo
  Kpi.kpiInscripcionesVendedoraCicloINBI(req.params.actual,(err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los departamentos"
      });
    else res.send(data);
  });
};

exports.kpiInscripcionesVendedoraCicloRangoFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraCicloRangoFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraCicloRangoINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraCicloRangoINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraCicloDiaFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraCicloDiaFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraCicloDiaINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraCicloDiaINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraRangoFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraRangoFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraRangoINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraRangoINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraDiaFAST = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraDiaFAST(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};


exports.kpiInscripcionesVendedoraDiaINBI = (req, res) => {
  //validamos que tenga algo el req.body
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({ message: "El Contenido no puede estar vacio" });
  }

  Kpi.kpiInscripcionesVendedoraDiaINBI(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear el departamento"
      })
    else res.status(200).send(data);
  })
};



exports.kpiInscripcionesPorCicloFAST = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloFast = await Kpi.TotalInscritosPorCicloFast(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursal = await Kpi.acumuladoPorSucursal(ciclo).then(response => response)
    const semanaPorSucursal = await Kpi.semanaPorSucursal(ciclo).then(response => response)
    const ayerPorSucursal = await Kpi.ayerPorSucursal(ciclo).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      let sucursal = semanaPorSucursal.find(el => el.plantel == element.plantel);
      let sucursalAyer = ayerPorSucursal.find(el => el.plantel == element.plantel);
      element.semanal = sucursal ? sucursal.cantidad : 0;
      element.ayer = sucursalAyer ? sucursalAyer.cantidad : 0;
    });

    res.send({totalFast:TotalInscritosPorCicloFast,tabla:acumuladoPorSucursal});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }

}



exports.kpiInscripcionesPorCicloINBI = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloINBI = await Kpi.TotalInscritosPorCicloINBI(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursal = await Kpi.acumuladoPorSucursal(ciclo).then(response => response)
    const semanaPorSucursal = await Kpi.semanaPorSucursal(ciclo).then(response => response)
    const ayerPorSucursal = await Kpi.ayerPorSucursal(ciclo).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      let sucursal = semanaPorSucursal.find(el => el.plantel == element.plantel);
      let sucursalAyer = ayerPorSucursal.find(el => el.plantel == element.plantel);
      element.semanal = sucursal ? sucursal.cantidad : 0;
      element.ayer = sucursalAyer ? sucursalAyer.cantidad : 0;
    });

    res.send({totalINBI:TotalInscritosPorCicloINBI,tabla:acumuladoPorSucursal});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }

}

/***********************************************************************************************************/
/***************************************** K P I -- G E N E R A L ******************************************/
/***********************************************************************************************************/

exports.kpiNiPorCicloGeneralFAST = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloFast = await Kpi.TotalInscritosPorCicloFast(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursal     = await Kpi.acumuladoPorSucursal(ciclo).then(response => response)
    const acumuladoPorSucursalAyer = await Kpi.acumuladoPorSucursalAyer(ciclo).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      // Buscamos primero el plantel, para ver si esta en los del ciclo actual
      let plantel = acumuladoPorSucursalAyer.find(el => el.plantel == element.plantel);
      // Si si esta, entonces sacamos la diferencia
      element.avance = plantel ? (plantel.acumulado) : 0;
    });

    res.send({totalFAST:TotalInscritosPorCicloFast,tabla:acumuladoPorSucursal});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }

}



exports.kpiNiPorCicloGeneralINBI = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloINBI = await Kpi.TotalInscritosPorCicloINBI(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursal     = await Kpi.acumuladoPorSucursal(ciclo).then(response => response)
    const acumuladoPorSucursalAyer = await Kpi.acumuladoPorSucursalAyer(ciclo).then(response => response)

    acumuladoPorSucursal.forEach(element => {
      // Buscamos primero el plantel, para ver si esta en los del ciclo actual
      let plantel = acumuladoPorSucursalAyer.find(el => el.plantel == element.plantel);
      // Si si esta, entonces sacamos la diferencia
      element.avance = plantel ? (plantel.acumulado) : 0;
    });

    res.send({totalINBI:TotalInscritosPorCicloINBI,tabla:acumuladoPorSucursal});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }

}


exports.kpiNiPorCicloGeneralTEENS = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloTEENS = await Kpi.TotalInscritosPorCicloTEENS(ciclo).then(response => response);

  try {
    
    const acumuladoPorSucursalTeens      = await Kpi.acumuladoPorSucursalTeens(ciclo).then(response => response)
    const acumuladoPorSucursalAyerTeens  = await Kpi.acumuladoPorSucursalAyerTeens(ciclo).then(response => response)

    acumuladoPorSucursalTeens.forEach(element => {
      // Buscamos primero el plantel, para ver si esta en los del ciclo actual
      let plantel = acumuladoPorSucursalAyerTeens.find(el => el.plantel == element.plantel);
      // Si si esta, entonces sacamos la diferencia
      element.avance = plantel ? (plantel.acumulado + element.acumulado) : 0;
    });

    res.send({totalINBI:TotalInscritosPorCicloTEENS,tabla:acumuladoPorSucursalTeens});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }

}

/**************************************************************************************************************/
/**************************************************************************************************************/
/**************************************************************************************************************/

exports.kpiNiVendedoraCicloFAST = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloFast = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response);
  let totalFast = 0
  for(const i in TotalInscritosPorCicloFast){
    totalFast += TotalInscritosPorCicloFast[i].total
  }

  try {
    
    const acumuladoPorVendedora     = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response)
    const acumuladoPorVendedoraAyer = await Kpi.acumuladoPorVendedoraAyer(ciclo).then(response => response)
    console.log(acumuladoPorVendedora,ciclo)
    acumuladoPorVendedora.forEach(element => {
      // Buscamos primero la vendedora, para ver si esta en los del ciclo actual
      let vendedora = acumuladoPorVendedoraAyer.find(el => el.nombre_completo == element.nombre_completo);
      // Si si esta, entonces sacamos la diferencia
      element.avance = vendedora ? (vendedora.total) : 0;
    });

    res.send({totalFAST:totalFast,tabla:acumuladoPorVendedora});
  } catch (error) {
    res.status(500).send({message:"Hubo un error" + error})
  }

}

exports.kpiNiVendedoraCicloTEENS = async (req, res)=>{
  let ciclo = req.params.ciclo;
  const TotalInscritosPorCicloTeens = await Kpi.kpiInscripcionesVendedoraCicloTEENS(ciclo).then(response => response);
  let totalFast = 0
  for(const i in TotalInscritosPorCicloTeens){
    totalFast += TotalInscritosPorCicloTeens[i].total
  }

  try {
    
    const acumuladoPorVendedoraTeens     = await Kpi.kpiInscripcionesVendedoraCicloTEENS(ciclo).then(response => response)
    const acumuladoPorVendedoraAyerTeens = await Kpi.acumuladoPorVendedoraAyerTeens(ciclo).then(response => response)
    acumuladoPorVendedoraTeens.forEach(element => {
      // Buscamos primero la vendedora, para ver si esta en los del ciclo actual
      let vendedora = acumuladoPorVendedoraAyerTeens.find(el => el.nombre_completo == element.nombre_completo);
      // Si si esta, entonces sacamos la diferencia
      element.avance = vendedora ? (vendedora.total) : 0;
    });

    res.send({totalFAST:totalFast,tabla:acumuladoPorVendedoraTeens});
  } catch (error) {
    res.status(500).send({message:"Hubo un error" + error})
  }

}



exports.kpiNiVendedoraCiclo = async (req, res)=>{
  let ciclo = req.params.ciclo;

  const TotalInscritosPorCicloInbi = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response);
  let totalFast = 0
  for(const i in TotalInscritosPorCicloInbi){
    totalInbi += TotalInscritosPorCicloInbi[i].total
  }

  try {
    
    const acumuladoPorVendedora     = await Kpi.kpiInscripcionesVendedoraCiclo(ciclo).then(response => response)
    const acumuladoPorVendedoraAyer = await Kpi.acumuladoPorVendedoraAyer(ciclo).then(response => response)

    acumuladoPorVendedora.forEach(element => {
      // Buscamos primero la vendedora, para ver si esta en los del ciclo actual
      let vendedora = acumuladoPorVendedoraAyer.find(el => el.nombre_completo == element.nombre_completo);
      // Si si esta, entonces sacamos la diferencia
      element.avance = vendedora ? (vendedora.total) : 0;
    });

    res.send({totalINBI:totalInbi,tabla:acumuladoPorVendedora});
  } catch (error) {
    res.status(500).send({message:"Hubo un error"})
  }

}

/*********************************************************************************************************/
/*********************************************************************************************************/
// RI por grupos

exports.kpiRiGrupos = async(req, res) => {
  try {
    // Primero sacamos a todos los grupos que están en ese ciclo y funcionando 
    const getGruposActual       = await Kpi.getGruposActual(req.params.inbi,req.params.fast).then(response => response)

    // Ahora a los alumnos de esos grupos
    const alumosCicloActual     = await Kpi.alumosCicloActual(req.params.inbi,req.params.fast).then(response => response)

    // Consultamos los alumnos que están en
    const alumosCicloSiguiente  = await Kpi.alumosCicloSiguiente(req.params.inbi,req.params.fast,req.params.sigInbi,req.params.sigFast).then(response => response)

    let resultadoFinal = []
    // Creado el arreglo list para recibir las cantidades
    for(const i in getGruposActual){
      let payload = {
        id_grupo                : getGruposActual[i].id_grupo,
        grupo                   : getGruposActual[i].grupo,
        alumnos_ciclo_actual    : 0,
        alumnos_siguiente_ciclo : 0,
        faltantes               : 0
      }
      resultadoFinal.push(payload)
    }

    // Sacamos los alumnos actuales
    for(const i in resultadoFinal){
      for(const j in alumosCicloActual){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloActual[j].id_grupo){
          resultadoFinal[i].alumnos_ciclo_actual += 1
        }
      }
    }

    // Sacamos los alumnos del siguiente ciclo
    for(const i in resultadoFinal){
      for(const j in alumosCicloSiguiente){
        // Solo hay que valdiar que el alumno sea de ese mismo grupo
        if(resultadoFinal[i].id_grupo == alumosCicloSiguiente[j].id_grupo){
          resultadoFinal[i].alumnos_siguiente_ciclo += 1
        }
      }
    }

    // Sacamos los alumnos faltantes
    for(const i in resultadoFinal){
      resultadoFinal[i].faltantes = resultadoFinal[i].alumnos_ciclo_actual - resultadoFinal[i].alumnos_siguiente_ciclo
    }

    res.send(resultadoFinal);
  } catch (error) {
    res.status(500).send({message:error})
  }
};