module.exports = app => {
    const archivos = require('../../controllers/lms/archivos.controllers') // const para utilizar nuestro controllador--> ADDED THIS

    //imagenes por grupo en lms fast
    app.get("/archivos.grupo.all", archivos.getArchivosGrupo); // OBTENER
    app.put("/archivos.grupo.update/:id", archivos.updateArchivoGrupo); //actualizar 

    //imagenes por grupo en lms inbimx
    app.get("/archivos.inbi.grupo.all", archivos.getArchivosGrupoInbi); // OBTENER
    app.put("/archivos.inbi.grupo.update/:id", archivos.updateArchivoGrupoInbi); //actualizar
};