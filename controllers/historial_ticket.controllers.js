var fs = require('fs');
const Historial_tickets = require("../models/historial_ticket.models.js");

// Crear un Historial_ticket
exports.addHistorial_ticket = (req, res) => {
  // Validacion de request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio"
    });
  }

  // Guardar el Historial_ticket en la BD
  Historial_tickets.addHistorial_ticket(req.body, (err, data) => {
    // EVALUO QUE NO EXISTA UN ERROR
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al crear la serie"
      })
    else res.send(data)
  })
};

exports.getHistorial_tickets = (req, res) => {
  Historial_tickets.getHistorial_tickets((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getHistorial_tickets_usuario = (req, res) => {
  Historial_tickets.getHistorial_tickets_usuario(req.params.idhistorial_ticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getHistorialTicketRespuesta = (req, res) => {
  Historial_tickets.getHistorialTicketRespuesta(req.params.idhistorial_ticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.putHistorialTicketRespuesta = (req, res) => {
  Historial_tickets.putHistorialTicketRespuesta(req.params.idhistorial_ticket, req.params.respuesta, req.params.idticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};


exports.putHistorial_tickets = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Historial_tickets.putHistorial_tickets(req.params.idhistorial_ticket, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la subserie con el id ${req.params.idhistorial_ticket }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la subserie con el id" + req.params.idhistorial_ticket
        });
      }
    } else res.send(data);
  });
}
//*************************************************************************/

exports.getHistorialEstatus = (req, res) => {
  Historial_tickets.getHistorialEstatus(req.params.idticket, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
}

exports.getCalificacionesAlumnos = (req, res) => {
  Historial_tickets.getCalificacionesAlumnos(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getCalificacionesAlumnosInbi = (req, res) => {
  Historial_tickets.getCalificacionesAlumnosInbi(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};



exports.putHistorial = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Historial_tickets.putHistorial(req.params.idhistorial_asistencia, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el historial de inasistencia con el id ${req.params.idhistorial_asistencia }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el historial de inasistencia con el id" + req.params.idhistorial_asistencia
        });
      }
    } else res.send(data);
  });
}


exports.putHistorialAceptar = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Historial_tickets.putHistorialAceptar(req.params.idhistorial_asistencia, req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el historial de inasistencia con el id ${req.params.idhistorial_asistencia }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el historial de inasistencia con el id" + req.params.idhistorial_asistencia
        });
      }
    } else res.send(data);
  });
}

/**********************************************************************************/
/**********************************************************************************/
exports.getRiesgoAlumno = async (req, res) => {
  const idCiclo = req.body.ciclo
  let data = []

  const alumnosActivos = await Historial_tickets
    .getAlumnosFAST(idCiclo)
    .then((response) => response)

  const alumnos = await Historial_tickets
    .getRiesgoAlumnosFast(idCiclo)
    .then((response) => response)

  const asistencias = await Historial_tickets
    .asistenciasFast(idCiclo)
    .then((response) => response)

    console.log( alumnos )

  for(const i in alumnos){

    // Crear el payload a cargar
    let payload = {
      ...alumnos[i],
      asistencia:   0,
      falta:        0,
      retardo:      0,
      justificada:  0
    }
      
    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  // Ahora revisamos los que nooo tienen actividades
  for(const i in alumnosActivos){
    let payload = {}
    const alumno = alumnos.find(el=> el.id_alumno == alumnosActivos[i].id_alumno)
    
    if(!alumno){
      // Crear el payload a cargar
      payload = {
        matricula:               alumnosActivos[i].matricula,
        nombre:                  alumnosActivos[i].nombre,
        id_alumno:               alumnosActivos[i].id_alumno,
        valor_asistencia:        alumnosActivos[i].valor_asistencia,
        telefono:                0,
        celular:                 0,
        id_curso:                alumnosActivos[i].id_curso,
        id_plantel:              alumnosActivos[i].id_plantel,
        valor_ejercicios:        alumnosActivos[i].valor_ejercicios,
        valor_examenes:          alumnosActivos[i].valor_examenes,
        id_grupo:                alumnosActivos[i].id_grupo,
        grupo:                   alumnosActivos[i].grupo,
        calificacion:            0,
        total_dias_laborales:    alumnosActivos[i].total_dias_laborales,
        idevaluacion:            0,
        num_examenes:            alumnosActivos[i].num_examenes,
        num_ejercicios:          alumnosActivos[i].num_ejercicios,
        tipo_evaluacion:         0,
        idCategoriaEvaluacion:   0,
        diferencia:              0,
        asistencia:              0,
        falta:                   0,
        retardo:                 0,
        justificada:             0,
        primera:                 '',
        segunda:                 '',
      }
    }

    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  res.send(data);
};

exports.getRiesgoAlumnoInbi = async (req, res) => {
  const idCiclo = req.body.ciclo
  let data = []

  const alumnosActivos = await Historial_tickets
    .getAlumnosINBI(idCiclo)
    .then((response) => response)

  const alumnos = await Historial_tickets
    .getRiesgoAlumnosInbi(idCiclo)
    .then((response) => response)

  const asistencias = await Historial_tickets
    .asistenciasInbi(idCiclo)
    .then((response) => response)


  for(const i in alumnos){

    // Crear el payload a cargar
    let payload = {
      ...alumnos[i],
      asistencia:   0,
      falta:        0,
      retardo:      0,
      justificada:  0
    }
      
    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  // Ahora revisamos los que nooo tienen actividades
  for(const i in alumnosActivos){
    let payload = {}
    const alumno = alumnos.find(el=> el.id_alumno == alumnosActivos[i].id_alumno)
    
    if(!alumno){
      // Crear el payload a cargar
      payload = {
        nombre:                  alumnosActivos[i].nombre,
        id_alumno:               alumnosActivos[i].id_alumno,
        valor_asistencia:        alumnosActivos[i].valor_asistencia,
        telefono:                0,
        celular:                 0,
        id_curso:                alumnosActivos[i].id_curso,
        id_plantel:              alumnosActivos[i].id_plantel,
        valor_ejercicios:        alumnosActivos[i].valor_ejercicios,
        valor_examenes:          alumnosActivos[i].valor_examenes,
        id_grupo:                alumnosActivos[i].id_grupo,
        grupo:                   alumnosActivos[i].grupo,
        calificacion:            0,
        total_dias_laborales:    alumnosActivos[i].total_dias_laborales,
        idevaluacion:            0,
        num_examenes:            alumnosActivos[i].num_examenes,
        num_ejercicios:          alumnosActivos[i].num_ejercicios,
        tipo_evaluacion:         0,
        idCategoriaEvaluacion:   0,
        diferencia:              0,
        asistencia:              0,
        falta:                   0,
        retardo:                 0,
        justificada:             0,
      }
    }

    let countAsistencia = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 1 
    })


    let countRetardo = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 3
    })

    let countFalta = asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo&&
      alumno.valor_asistencia  == 2
    })

    let countJustificada =  asistencias.filter((alumno)=>{
      return alumno.id_usuario == payload.id_alumno &&
      alumno.id_grupo          == payload.id_grupo &&
      alumno.valor_asistencia  == 4
    })

    payload.asistencia  = countAsistencia.length
    payload.falta       = countFalta.length
    payload.retardo     = countRetardo.length
    payload.justificada = countJustificada.length

    data.push(payload)
  }

  res.send(data);
};

exports.getRiesgoAlumnoRegistro = (req, res) => {
  Historial_tickets.getRiesgoAlumnoRegistro((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getRiesgoAlumnoInbiRegistro = (req, res) => {
  Historial_tickets.getRiesgoAlumnoInbiRegistro((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};

exports.getAsistenciaAlumnoGrupo = (req, res) => {
  Historial_tickets.getAsistenciaAlumnoGrupo(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Se produjo algún error al recuperar los Historial_tickets"
      });
    else res.send(data);
  });
};