var fs = require('fs');
const Areas = require("../models/areas.model.js");

// Crear un cliente
exports.addArea = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Areas.addArea(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la serie"
  		})
  	else res.send(data)
  })
};

exports.getAreas = (req,res) => {
    Areas.getAreas((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};


exports.putAreas = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	Areas.putAreas(req.params.idareas_ticket, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la subserie con el id ${req.params.idareas_ticket }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la subserie con el id" + req.params.idareas_ticket 
				});
			}
		} 
		else res.send(data);
	});
}
