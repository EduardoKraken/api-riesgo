module.exports = app => {
  const historial_tickets = require('../controllers/historial_ticket.controllers') // --> ADDED THIS
  app.post("/historial_tickets.add",                           historial_tickets.addHistorial_ticket); // CREAR UN NUEVO GRUPO
  app.get("/historial_tickets.all",                            historial_tickets.getHistorial_tickets);
  app.get("/historial_tickets.allUsuario/:idhistorial_ticket", historial_tickets.getHistorial_tickets_usuario);
  app.put("/historial_tickets.update/:idhistorial_ticket",     historial_tickets.putHistorial_tickets);

  app.get("/historial_tickets.estatus/:idticket",              historial_tickets.getHistorialEstatus);


  app.get("/historial_ticket.responder_ticket/:idhistorial_ticket",                         historial_tickets.getHistorialTicketRespuesta);
  app.get("/verhistorialticketusuario/:idhistorial_ticket",                                 historial_tickets.getHistorialTicketRespuesta);
  app.put("/puthistorial_ticket.responder_ticket/:idhistorial_ticket/:respuesta/:idticket", historial_tickets.putHistorialTicketRespuesta);

  app.post("/calificaciones.alumnos",                           historial_tickets.getRiesgoAlumno);
  app.post("/calificaciones.alumnos.inbi",                      historial_tickets.getRiesgoAlumnoInbi);
  app.put("/historial.update/:idhistorial_asistencia",          historial_tickets.putHistorial);
  app.put("/historial.update.aceptar/:idhistorial_asistencia",  historial_tickets.putHistorialAceptar);

  app.post("/riesgo.alumnos",                                   historial_tickets.getRiesgoAlumno);
  app.post("/riesgo.alumnos.inbi",                              historial_tickets.getRiesgoAlumnoInbi);
  
  app.post("/riesgo.alumnos.registro",                          historial_tickets.getRiesgoAlumnoRegistro);
  app.post("/riesgo.alumnos.registro.inbi",                     historial_tickets.getRiesgoAlumnoInbiRegistro);
  app.post("/asistencias.alumno.grupo",                         historial_tickets.getAsistenciaAlumnoGrupo);
   
  //app.delete("/historial_tickets.delete/:idjefe_area", historial_tickets.delHistorial_tickets);
};